#pragma once
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include <vector>
#include <stdint.h>
#include "kinematics.h"

#define CERR_ERR(x) std::cerr << "\033[22;31;1m" << x << "\033[0m"<< std::endl;
#define CERR_INFO(x) std::cerr << "\033[22;34;1m" << x << "\033[0m" << std::endl;
#define CERR_INFO2(x,y) std::cerr << "\033[22;32;1m" << x << y << "\033[0m" << std::endl;
#define CERR_PARAMS(x,y) std::cerr << "\033[22;35;1m" << x << y << "\033[0m" << std::endl;
#define CERR_DEBUG(x) std::cerr << "\033[0;32;1m" << x << "\033[0m" << std::endl;
#define CERR_WARN(x) std::cerr << "\033[0;33;1m" << x << "\033[0m" << std::endl;
#define CERR_SPECIAL(x) std::cerr << "\033[0;35;1m" << x << "\033[0m" << std::endl;

namespace common_fnc{

Eigen::Matrix3f v2t(const Eigen::Vector3f &v);
Eigen::Vector3f t2v(const Eigen::Matrix3f &t);
void normalizeTheta(float &theta);
float quaternion2Theta(const Eigen::Vector4f &q);
bool checkDistance(const std::vector<float> &last, const std::vector<float> &prev);

template<typename T> T sign_fnc(const T& in){
    if (in > (T)0)
        return (T)1;
    else
        return (T)(-1);
}

bool motionTerminated(const Eigen::Vector3f &init, const Eigen::Vector3f &current, kinematics::Motion& curr_mot);

void std2Eigen(const std::vector<std::vector<float> >&, Eigen::MatrixXf&);

void selectNextMotion(int&, std::vector<kinematics::Motion>&, const Eigen::MatrixXf&);

void printBanner(const char** banner);
void printParameters(const Eigen::VectorXf& params, const std::string& robot);

//Odometry computation Refinement
const float EPSILON=1e-6;
inline float sinThetaOverTheta(float theta) {
  if (fabs(theta)<EPSILON)
    return 1;
  return sin(theta)/theta;
}
inline float oneMinisCosThetaOverTheta(float theta) {
  if (fabs(theta)<EPSILON)
    return 0;
  return (1.0f-cos(theta))/theta;
}
const float cos_coeffs[]={0., 0.5 ,  0.    ,   -1.0/24.0,   0.    , 1.0/720.0};
const float sin_coeffs[]={1., 0.  , -1./6. ,      0.    ,   1./120, 0.   };
void computeThetaTerms(float& sin_theta_over_theta, float& one_minus_cos_theta_over_theta, float theta);

}
