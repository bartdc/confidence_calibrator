#include "ros/ros.h"

#include <Eigen/Core>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
//#include <geometry_msgs/TransformStamped.h>
#include "utils.h"
#include "kinematics.h"
#include <fstream>

using namespace std;
using namespace common_fnc;
using namespace kinematics;

struct Manager{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    //odometry topic stuff
    std::string odom_topic;
    std::string odom_frame_id;
    //odometry tf stuff
    std::string tf_frame_id;
    std::string tf_child_frame_id;
    //sensor tf stuff
    ros::Time old_time;
    geometry_msgs::TransformStamped laser_baselink_tf;
    tf::TransformBroadcaster laser_baselink_broadcaster;
    std::string sensor_tf_child_frame_id;

    //other stuff
    Eigen::VectorXf params;
    std::string robot;
    ros::Publisher pub;
    tf::TransformBroadcaster odom_broadcaster;
    Eigen::Vector3f pose;
    Kinematics* kin;
    Eigen::VectorXf old_ticks;
    std::vector<int> tick_id;
    bool firstMessage;
    int pub_count;
};

Manager* man;

void odomCallback(const sensor_msgs::JointStateConstPtr &encoder){
    std::cerr<<"x";


    if(!man->firstMessage){
        std::cerr<<"o";
        Eigen::VectorXf current_ticks;

        if(man->robot == "differential_drive"){
            float left = encoder->position[man->tick_id[0]] - man->old_ticks(0);
            normalizeTheta(left);
            float right = encoder->position[man->tick_id[1]] - man->old_ticks(1);
            normalizeTheta(right);
            man->old_ticks(0) = encoder->position[man->tick_id[0]];
            man->old_ticks(1) = encoder->position[man->tick_id[1]];
            current_ticks = Eigen::VectorXf::Zero(2);
            current_ticks << left, right;

        }else if(man->robot == "youbot"){

            float fl = encoder->position[man->tick_id[0]] - man->old_ticks(0);
            normalizeTheta(fl);
            float fr = encoder->position[man->tick_id[1]] - man->old_ticks(1);
            normalizeTheta(fr);
            float bl = encoder->position[man->tick_id[2]] - man->old_ticks(2);
            normalizeTheta(bl);
            float br = encoder->position[man->tick_id[3]] - man->old_ticks(3);
            normalizeTheta(br);
            man->old_ticks(0) = encoder->position[man->tick_id[0]];
            man->old_ticks(1) = encoder->position[man->tick_id[1]];
            man->old_ticks(2) = encoder->position[man->tick_id[2]];
            man->old_ticks(3) = encoder->position[man->tick_id[3]];
            current_ticks = Eigen::VectorXf::Zero(4);
            current_ticks << fl, fr, bl, br;

        }


        //compute odometry
        man->kin->computeOdomRobotStep(man->pose, current_ticks, man->params);
        geometry_msgs::Quaternion quat = tf::createQuaternionMsgFromYaw(man->pose(2));

        //publish odometry
        nav_msgs::Odometry odom_msg;
        odom_msg.header.stamp = encoder->header.stamp;
        odom_msg.header.frame_id = man->odom_frame_id;
        odom_msg.header.seq = man->pub_count++;
        odom_msg.pose.pose.position.x = man->pose(0);
        odom_msg.pose.pose.position.y = man->pose(1);
        odom_msg.pose.pose.orientation = quat;
        man->pub.publish(odom_msg);

        //publish tf
        geometry_msgs::TransformStamped odom_tf;
        odom_tf.header.stamp = encoder->header.stamp;
        odom_tf.header.frame_id = man->tf_frame_id;
        odom_tf.child_frame_id = man->tf_child_frame_id;
        odom_tf.transform.translation.x = man->pose(0);
        odom_tf.transform.translation.y = man->pose(1);
        odom_tf.transform.rotation = quat;
        man->odom_broadcaster.sendTransform(odom_tf);

        //ones every 5 sec publish laser-baselink tf
        float exp_time = (encoder->header.stamp - man->old_time).toSec();
        if(exp_time > 5){
            man->laser_baselink_tf.header.stamp = encoder->header.stamp; //update time
            man->laser_baselink_broadcaster.sendTransform(man->laser_baselink_tf);
            man->old_time = encoder->header.stamp;
        }
    }


    else { //first Message received
        man->firstMessage = false;
        man->old_time = encoder->header.stamp;

        if(man->robot == "differential_drive")
        {
            int left_enc_id = 0;
            int right_enc_id = 1;
            int l = encoder->name.size();
            for(int i=0; i<l; i++)
            {
                std::string current_joint = encoder->name[i];
                if(!strcmp(current_joint.c_str(), "Pioneer_p3dx_leftMotor"))
                    left_enc_id = i;
                else if(!strcmp(current_joint.c_str(), "Pioneer_p3dx_rightMotor"))
                    right_enc_id = i;
            }
            man->tick_id.push_back(left_enc_id);
            man->tick_id.push_back(right_enc_id);

            man->old_ticks(0) = encoder->position[left_enc_id];
            man->old_ticks(1) = encoder->position[right_enc_id];

        }
        else if(man->robot == "youbot")
        {
            int fl_enc_id = 0;
            int fr_enc_id = 1;
            int bl_enc_id = 2;
            int br_enc_id = 3;

            //search for encoder id in the array
            int l = encoder->name.size();
            for(int i=0; i<l; i++)
            {
                std::string current_joint = encoder->name[i];
                if(current_joint == "rollingJoint_fl" || current_joint == "wheel_joint_fl"){
                    fl_enc_id = i;
                }
                else if(current_joint == "rollingJoint_fr" || current_joint == "wheel_joint_fr"){
                    fr_enc_id = i;
                }
                else if(current_joint == "rollingJoint_rl" || current_joint == "wheel_joint_bl"){
                    bl_enc_id = i;
                }
                else if(current_joint == "rollingJoint_rr" || current_joint == "wheel_joint_br"){
                    br_enc_id = i;

                }

            }

            man->tick_id.push_back(fl_enc_id);
            man->tick_id.push_back(fr_enc_id);
            man->tick_id.push_back(bl_enc_id);
            man->tick_id.push_back(br_enc_id);

            man->old_ticks(0) = encoder->position[fl_enc_id];
            man->old_ticks(1) = encoder->position[fr_enc_id];
            man->old_ticks(2) = encoder->position[bl_enc_id];
            man->old_ticks(3) = encoder->position[br_enc_id];

        }
    }
}



const char* banner[]={
    "\nOdom_Publisher",
    " takes a parameter file (generated from calibrator node) and publishes the odometry",
    "",
    "usage: confidence_calibrator odom_publisher [options] <filepath>",
    "Options: ",
    " _param_file:=               [string], param file as generated by the calibrator node, default ['']",
    " ***TOPIC***",
    " _encoder_topic:=            [string <sensor_msgs::JointState>], the encoder data coming from platform, default [/joint_states]",
    " _odom_topic_published:=     [string <nav_msgs::Odometry>], the pose of the robot as computed with the given parameters, default [/calib_odom] ",
    " _odom_frame_id:=            [string], the frame_id you want to set for /calib_odometry messages, default [calib_odom]",
    " _tf_frame_id:=              [string], the frame_id you want to set for the odom_tf_broadcaster, default [calib_odom]",
    " _tf_child_frame_id:=        [string], the child_frame_id you want to set for the odom_tf_broadcaster, default [base_link]",
    " _sensor_tf_child_frame_id:= [string], the child_frame_id you want to set for the sensor_tf_broadcaster, default [sensor]",
    0
};




int main(int argc, char** argv)
{
    ros::init(argc, argv, "odom_publisher");
    ros::NodeHandle nh("~");

    man = new Manager;

    std::string param_file, encoder_topic;

    for(int i=0; i<argc; i++){
        if(!strcmp(argv[i],"-h")){
            printBanner(banner);
            return 0;
        }
    }

    nh.param<std::string>("param_file", param_file, std::string(""));
    nh.param<std::string>("encoder_topic", encoder_topic, std::string("/joint_states"));
    nh.param<std::string>("odom_topic_published", man->odom_topic, std::string("/calib_odom"));
    nh.param<std::string>("odom_frame_id", man->odom_frame_id, std::string("calib_odom"));
    nh.param<std::string>("tf_frame_id", man->tf_frame_id, std::string("calib_odom"));
    nh.param<std::string>("tf_child_frame_id", man->tf_child_frame_id, std::string("base_link"));
    nh.param<std::string>("sensor_tf_child_frame_id", man->sensor_tf_child_frame_id, std::string("sensor"));


    CERR_INFO("============TOPIC NAMEs");
    std::cerr <<"_param_file:= "<<param_file<<std::endl;
    std::cerr <<"_encoder_topic:= "<<encoder_topic<<std::endl;
    std::cerr <<"_odom_topic_published:= "<<man->odom_topic<<std::endl;
    std::cerr <<"_odom_frame_id:= "<<man->odom_frame_id<<std::endl;
    std::cerr <<"_tf_frame_id:= "<<man->tf_frame_id<<std::endl;
    std::cerr <<"_tf_child_frame_id:= "<<man->tf_child_frame_id<<std::endl;


    std::ifstream file;
    file.open(param_file.c_str(), std::ios::in);
    if(!file)
    {
        CERR_ERR("Error while opening param_file!");
        return -1;
    }

    std::string line, mode, param_i;

    //[MODE]
    std::getline(file, line);
    std::getline(file, mode);
    CERR_INFO2(line, mode);

    //[ROBOT]
    std::getline(file, line);
    std::getline(file, man->robot);
    CERR_INFO2(line, man->robot);

    //read params
    int num_params;
    if(man->robot == "differential_drive"){
           num_params = 6;
            man->kin = new DifferentialDrive();
            man->old_ticks = Eigen::VectorXf::Zero(2);
    }
    else if(man->robot == "youbot"){
            num_params = 8;
            man->kin = new Youbot();
            man->old_ticks = Eigen::VectorXf::Zero(4);
    }
    else{
           CERR_ERR("Error while reading robot type [differential_drive/youbot]");
           return -1;
    }
    //[PARAMS]
    std::getline(file, line);
    man->params = Eigen::VectorXf::Zero(num_params);
    for(int i=0; i<num_params; i++){
        std::getline(file, param_i);
        CERR_INFO2("param_i = ", param_i);
        man->params(i) = (float)atof(param_i.c_str());
    }

    ros::Subscriber encoder_sub = nh.subscribe(encoder_topic,1000, odomCallback);
    man->pub = nh.advertise<nav_msgs::Odometry>(man->odom_topic,1000);
    man->pose = Eigen::Vector3f::Zero();
    man->firstMessage = true;
    man->pub_count = 0;

    //populate static transform between baselink and laser
    man->laser_baselink_tf.header.frame_id = man->tf_child_frame_id; //the sensor is attached to the platform
    man->laser_baselink_tf.child_frame_id = man->sensor_tf_child_frame_id;
    man->laser_baselink_tf.transform.translation.x = man->params(0); //sensor-x
    man->laser_baselink_tf.transform.translation.y = man->params(1); //sensor-y
    geometry_msgs::Quaternion q = tf::createQuaternionMsgFromYaw(man->params(2));
    man->laser_baselink_tf.transform.rotation = q;



    ROS_INFO("initialization complete!");

    ros::spin();

    return 0;

}
