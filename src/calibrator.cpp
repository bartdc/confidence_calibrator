#include "calibrator.h"
#include "utils.h"


using namespace common_fnc;
using namespace kinematics;

namespace calibration{

Calibrator::Calibrator(int iteration){
    _chi2 = 0.0f;
    _params_number = 0;
    _eps = 1e-3;
    _iterations = iteration;
    _my_robot = NULL;
    _data_dim = 0;
    _min_chi2_norm = 1e6;
    _chi2_min_it = 0;
}

Calibrator::~Calibrator(){}

Eigen::MatrixXf Calibrator::computeJacobian(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks){

    Eigen::MatrixXf J(3,_params_number);
    J = Eigen::MatrixXf::Zero(3,_params_number);

    Eigen::MatrixXf perturbated_params(_params_number,_params_number);
    perturbated_params = Eigen::MatrixXf::Identity(_params_number,_params_number);

    //TODO: compute these inv(v2t)*v2t
    for (int i = 0; i < _params_number; ++i){
        J.col(i) = computePerturbError(Z,ticks,i+1) - computePerturbError(Z,ticks,-(i+1));
    }
    return .5*J/_eps;
}

Eigen::Vector3f Calibrator::computePerturbError(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks, const int& id){

    int param = abs(id) - 1;
    float backup_param = _X_o(param);
    int id_sign = sign_fnc(id);
    float perturbation = id_sign*_eps;
    if (param > 2) //simply sum
        _X_o(param) += perturbation;
    else //manifold
    {
        Eigen::Vector3f vet_perturbation(Eigen::Vector3f::Zero());
        vet_perturbation(param) = perturbation;
        Eigen::Matrix3f man_perturbation = v2t(vet_perturbation);

        _X_o.head(3) = t2v(v2t(_X_o.head(3))*man_perturbation);
    }
    Eigen::Vector3f error =  computeError(Z, ticks); //compute error
    _X_o(param) = backup_param; //restore unperturbated params

    return error;

}


Eigen::Vector3f Calibrator::computeError(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks){

    Eigen::Vector3f robot_motion(Eigen::Vector3f::Zero());
    _my_robot->computeOdomRobotStep(robot_motion, ticks, _X_o);
    Eigen::Matrix3f KL = v2t(_X_o.head(3));
    Eigen::Matrix3f Z_pred = KL.inverse()*v2t(robot_motion)*KL;

    return t2v(Z.inverse()*Z_pred);
}


void Calibrator::calibrate(const Eigen::MatrixXf &pose, const Eigen::MatrixXf &ticks){

    //extract/separate data
    _data_dim = pose.rows();
    int ticks_dim = ticks.cols();

    int odom_params_num = _params_number - 3;

    Eigen::MatrixXf conditioner = _my_robot->getConditioner();
    double alpha = 1;

    //simulation starts
    for(int it=0; it < _iterations; ++it)
    {
        Eigen::Matrix3f KL;
        KL = v2t((Eigen::Vector3f(_X_o(0), _X_o(1), _X_o(2))));
        Eigen::VectorXf Ko(odom_params_num);
        Ko = Eigen::VectorXf::Zero(odom_params_num);
        Ko = _X_o.tail(odom_params_num);

        Eigen::VectorXf b(Eigen::VectorXf::Zero(_params_number));
        _H_mat = (Eigen::MatrixXf::Zero(_params_number,_params_number));

        _chi2 = 0;

        for(int i=1; i < _data_dim; ++i)
        {
            //compute error
            Eigen::Vector3f prev_pose(pose(i-1,0),pose(i-1,1),pose(i-1,2));
            Eigen::Vector3f current_pose(pose(i,0),pose(i,1),pose(i,2));
            Eigen::Matrix3f pose_step = v2t(prev_pose).inverse()*v2t(current_pose);
            Eigen::VectorXf tick_step(ticks.row(i) - ticks.row(i-1));
            for(int i=0; i<odom_params_num-1; ++i)
                normalizeTheta(tick_step(i));
            Eigen::Vector3f err = computeError(pose_step, tick_step);
            // update chi2
            _chi2 += err.transpose()*err;

            //robust kernel
            alpha = 1;
            // if(_chi2 > max_err)
            // {
            //     alpha = sqrt(max_err/chis);
            // }
            Eigen::MatrixXf J = alpha*computeJacobian(pose_step, tick_step);
            Eigen::Matrix3f Omega(Eigen::Matrix3f::Identity());
            //Omega(2,2) = 10;

            //update H
            _H_mat += J.transpose()*Omega*J;
            //update b
            b += J.transpose()*Omega*err;
        }

        Eigen::VectorXf deltaX(_params_number);
        deltaX = - (_H_mat + conditioner).jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);

        normalizeTheta(deltaX(2));

        KL = KL*v2t(Eigen::Vector3f(deltaX(0), deltaX(1), deltaX(2)));
        Ko = Ko + deltaX.tail(odom_params_num);


        _X_o.head(3) = t2v(KL);
        _X_o.tail(odom_params_num) = Ko;

        CERR_INFO2("Normalized chi2: ",_chi2/_data_dim);
    }


    Eigen::EigenSolver<Eigen::MatrixXf> hes(_H_mat);
    _eigvet = hes.eigenvectors();
    _eigval = hes.eigenvalues();

}


void Calibrator::reset(){

    _H_mat = Eigen::MatrixXf::Zero(_params_number,_params_number);
    _chi2 = 0;
    _data_dim = 0;

}



}
