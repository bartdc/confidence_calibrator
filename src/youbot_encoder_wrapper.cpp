#include "ros/ros.h"

#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include "utils.h"

using namespace std;
using namespace common_fnc;


ros::Publisher encoder_pub;

void encoderCallback(const sensor_msgs::JointStateConstPtr &encoder){

    if(encoder->name[0] == "wheel_joint_fl")
    { ///RE-PUBLISH
        sensor_msgs::JointState j = *encoder;
        encoder_pub.publish(j);
    }

}


const char* banner[]={
    "\nYoubot Encoder Wrapper",
    " republishes the encoder data of the KUKA Youbot to manage wheel ticks only",
    "",
    "usage: confidence_calibrator youbot_encoder_wrapper [options]",
    "Options: ",
    " ***TOPIC***",
    " _encoder_topic:=            [string <sensor_msgs::JointState>], the encoder data coming from platform, default [/joint_states]",
    " _output_topic:=             [string <nav_msgs::Odometry>], the republished ticks, default [/youbot_wheel_joints] ",
    0
};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "youbot_encoder_wrapper");
    ros::NodeHandle nh("~");

    for(int i=0; i<argc; i++){
        if(!strcmp(argv[i],"-h")){
            printBanner(banner);
            return 0;
        }
    }

    std::string encoder_topic, output_topic;

    nh.param<std::string>("encoder_topic", encoder_topic, std::string("/joint_states"));
    nh.param<std::string>("output_topic", output_topic, std::string("/youbot_wheel_joints"));

    CERR_INFO("============TOPIC NAMEs");
    std::cerr <<"_encoder_topic:= "<<encoder_topic<<std::endl;
    std::cerr <<"_output_topic:= "<<output_topic<<std::endl;

    encoder_pub = nh.advertise<sensor_msgs::JointState>(output_topic,1000);
    ros::Subscriber encoder_sub = nh.subscribe(encoder_topic, 1000, encoderCallback);

    ROS_INFO("initialization complete!");

    ros::spin();

    return 0;

}
