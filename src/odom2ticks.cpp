#include "ros/ros.h"

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include "utils.h"
#include "kinematics.h"

using namespace std;
using namespace common_fnc;
using namespace kinematics;

struct Odom2ticks{
    ros::Publisher _diff_drive_pub;
    //ros::Publisher _vrep_left_diff_drive_pub;
    //ros::Publisher _vrep_right_diff_drive_pub;
    Eigen::Matrix3f _init_pose;
    Eigen::Matrix3f _current_pose;
    Eigen::Vector3f _odom_params;
    Eigen::Vector2f _curr_ticks;
    bool _firstMessage;
    DifferentialDrive* dd;
} odom2ticks;


void publishTicks(const std::string& s, const Eigen::Matrix3f& var, const ros::Time& time){

    Eigen::Vector2f estimated_ticks(Eigen::Vector2f::Zero());
    estimated_ticks = odom2ticks.dd->predictTicks(var,Eigen::Matrix3f::Identity(),odom2ticks._odom_params);
    odom2ticks._curr_ticks += estimated_ticks;

    if(s == "diff_drive")
    {
        std::vector<double> j_ticks;
        j_ticks.push_back((double)odom2ticks._curr_ticks(0));
        j_ticks.push_back((double)odom2ticks._curr_ticks(1));
        sensor_msgs::JointState joint_msg;
        joint_msg.header.stamp = time;
        joint_msg.position = j_ticks;
        odom2ticks._diff_drive_pub.publish(joint_msg);
    }
//    else if(s == "vrep_diff_drive")
//    {
//        sensor_msgs::JointState joint_msg_left, joint_msg_right;
//        joint_msg_left.header.stamp = time;
//        joint_msg_right.header.stamp = time;
//        std::vector<double> left_ticks, right_ticks;
//        left_ticks.push_back((double)odom2ticks._curr_ticks(0));
//        right_ticks.push_back((double)odom2ticks._curr_ticks(1));
//        joint_msg_left.position = left_ticks;
//        joint_msg_right.position = right_ticks;
//        odom2ticks._vrep_left_diff_drive_pub.publish(joint_msg_left);
//        odom2ticks._vrep_right_diff_drive_pub.publish(joint_msg_right);
//    }

}


void odomCallback(const nav_msgs::OdometryConstPtr &odom){

    float x = odom->pose.pose.position.x;
    float y = odom->pose.pose.position.y;
    float qx = odom->pose.pose.orientation.x;
    float qy = odom->pose.pose.orientation.y;
    float qz = odom->pose.pose.orientation.z;
    float qw = odom->pose.pose.orientation.w;
    float theta = quaternion2Theta(Eigen::Vector4f(qw,qx,qy,qz));
    ros::Time current_time = odom->header.stamp;


    if(!odom2ticks._firstMessage)
    {
        odom2ticks._current_pose = v2t(Eigen::Vector3f(x,y,theta));
        Eigen::Matrix3f delta_pose = odom2ticks._init_pose.inverse()*odom2ticks._current_pose;
        //Eigen::Vector3f variation = t2v(delta_pose);
        publishTicks("diff_drive", delta_pose, current_time);
        odom2ticks._init_pose = odom2ticks._current_pose;
    }
    else
    {
        odom2ticks._firstMessage = false;
        odom2ticks._init_pose = v2t(Eigen::Vector3f(x,y,theta));
    }

}

void vrepPoseCallback(const geometry_msgs::PoseStampedConstPtr &odom){

    float x = odom->pose.position.x;
    float y = odom->pose.position.y;
    float qx = odom->pose.orientation.x;
    float qy = odom->pose.orientation.y;
    float qz = odom->pose.orientation.z;
    float qw = odom->pose.orientation.w;
    float theta = quaternion2Theta(Eigen::Vector4f(qw,qx,qy,qz));
    ros::Time current_time = odom->header.stamp;

    if(!odom2ticks._firstMessage)
    {
        odom2ticks._current_pose = v2t(Eigen::Vector3f(x,y,theta));
        Eigen::Matrix3f delta_pose = odom2ticks._init_pose.inverse()*odom2ticks._current_pose;
        //Eigen::Vector3f variation = t2v(delta_pose);
        publishTicks("diff_drive", delta_pose, current_time);
        odom2ticks._init_pose = odom2ticks._current_pose;
    }
    else
    {
        odom2ticks._firstMessage = false;
        odom2ticks._init_pose = v2t(Eigen::Vector3f(x,y,theta));
    }

}

const char* banner[]={
    "\n Odom To Ticks",
    " simulated the kinematics of a differential drive robot publishing the ticks given the odometry",
    "",
    "usage: confidence_calibrator odom2ticks [options]",
    "Options: ",
    " ***TOPIC***",
    " _odom_topic:=               [string <nav_msgs::Odometry>], the odometry of the platform, default [/odom]",
    " _odom_pose_topic:=          [string <geometry_msgs::PoseStamped>], the odometry of the platform if provided as PoseStamped, default [/odometry] ",
    " _kl:=                       [float], a reasonable guess for the left wheel radius in meter, default [0.1] ",
    " _kr:=                       [float], a reasonable guess for the right wheel radius in meter, default [0.1] ",
    " _bl:=                       [float], a reasonable guess for the baseline in meter, default [0.3] ",
    0
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "odom2ticks");
    ros::NodeHandle nh("~");

    for(int i=0; i<argc; i++){
        if(!strcmp(argv[i],"-h")){
            printBanner(banner);
            return 0;
        }
    }

    float kl,kr,kbaseline;
    std::string odom_topic, odom_pose_topic;

    nh.param<std::string>("odom_topic", odom_topic, "/odom");
    nh.param<std::string>("odom_pose_topic", odom_pose_topic, "/odometry");
    nh.param<float>("kl", kl, 0.1);
    nh.param<float>("kr", kr, 0.1);
    nh.param<float>("kbaseline", kbaseline, 0.3);

    std::cout<<"\n_odom_topic(if nav_msgs/Odometry):= "<<odom_topic;
    std::cout<<"\n_odom_pose_topic(if geometry_msgs/PoseStamped):= "<<odom_pose_topic;
    std::cout<<"\n_kl:= "<<kl;
    std::cout<<"\n_kr:= "<<kr;
    std::cout<<"\n_kbaseline:= "<<kbaseline<<std::endl;


    odom2ticks._diff_drive_pub = nh.advertise<sensor_msgs::JointState>("/joint_states", 1000);
    //odom2ticks._vrep_left_diff_drive_pub = nh.advertise<sensor_msgs::JointState>("/vrep/LEFT_ticks", 1000);
    //odom2ticks._vrep_right_diff_drive_pub = nh.advertise<sensor_msgs::JointState>("/vrep/RIGHT_ticks", 1000);
    ros::Subscriber odom_sub = nh.subscribe(odom_topic,1000, odomCallback);
    ros::Subscriber vrep_pose_sub = nh.subscribe(odom_pose_topic,1000, vrepPoseCallback);

    odom2ticks.dd = new DifferentialDrive();
    odom2ticks._firstMessage = true;
    odom2ticks._odom_params = Eigen::Vector3f(kl,kr,kbaseline);
    odom2ticks._init_pose = Eigen::Matrix3f::Identity();
    odom2ticks._curr_ticks = Eigen::Vector2f::Zero();

    ROS_INFO("intialization complete!");

    ros::spin();

    return 0;

}
