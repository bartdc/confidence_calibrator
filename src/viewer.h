#pragma once
#include <QGLViewer/qglviewer.h>
#include <Eigen/Dense>


class Viewer : public QGLViewer
{
  protected: 

    void init();
    void draw();

};