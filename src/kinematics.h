#pragma once

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>

namespace kinematics{

	class Motion{
		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW
		    Motion(const float& x, const float& y, const float& th, const float& dx, const float& dd);
		    void setH(const Eigen::MatrixXf & H){_H = H;}
		    void setEigenVectors(const Eigen::MatrixXcf & eigvet){_eigvet = eigvet;}
		    void setEigenValues(const Eigen::MatrixXcf & eigval){_eigval = eigval;}
		    Eigen::MatrixXf getH(){return _H;}
		    Eigen::MatrixXcf getEigenVectors(){return _eigvet;}
		    Eigen::VectorXcf getEigenValues(){return _eigval;}
		    float getLinX(){return _lin_x;}
		    float getLinY(){return _lin_y;}
		    float getAng(){return _ang;}
		    float getLinDist(){return _lin_distance;}
		    float getRadDist(){return _rad_distance;}
		    Eigen::MatrixXf _H;
		    Eigen::MatrixXcf _eigvet;
		    Eigen::VectorXcf _eigval;


		private:	
		    float _lin_x;
		    float _lin_y;
		    float _ang;
		    float _lin_distance;
		    float _rad_distance;

	};


	class Kinematics{
		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			Kinematics();
			~Kinematics();
			void setStartSensorPose(const Eigen::Vector3f &start_sensor_pose){_start_sensor_pose = start_sensor_pose;}
            virtual Eigen::MatrixXf computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params) = 0;
            virtual void computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &ticks, const Eigen::VectorXf &params) = 0;
			virtual Eigen::MatrixXf getConditioner() = 0;
			virtual std::vector<Motion> getMotions() = 0;
		protected:
			Eigen::Vector3f _start_sensor_pose; //sensor frame

	};


	class DifferentialDrive: public Kinematics{
		public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			DifferentialDrive();
			~DifferentialDrive();
            Eigen::MatrixXf computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params);
            void computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &ticks, const Eigen::VectorXf &params);
			Eigen::MatrixXf getConditioner();
			std::vector<Motion> getMotions();
            Eigen::Vector2f predictTicks(const Eigen::Matrix3f& L, const Eigen::Matrix3f& KL, const Eigen::Vector3f& Ko);


	};


	class Omnidirectional: public Kinematics{
		public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			Omnidirectional();
			~Omnidirectional();
            virtual Eigen::MatrixXf computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params) = 0;
            virtual void computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &ticks, const Eigen::VectorXf &params) = 0;
			virtual Eigen::MatrixXf getConditioner() = 0;
			std::vector<Motion> getMotions();

	};


	class Youbot: public Omnidirectional{
		public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			Youbot();
			~Youbot();
            Eigen::MatrixXf computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params);
            void computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &ticks, const Eigen::VectorXf &params);
			Eigen::MatrixXf getConditioner();

	};


}
