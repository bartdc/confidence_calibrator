#include "kinematics.h"
#include "utils.h"
#include <iostream>

using namespace common_fnc;

namespace kinematics{

Kinematics::Kinematics(){}
Kinematics::~Kinematics(){}

Motion::Motion(const float& x, const float& y, const float& th, const float& dx, const float& dd)
{
    _lin_x = x;
    _lin_y = y;
    _ang = th;
    _lin_distance = dx;
    _rad_distance = dd;
}

/// DIFFERENTIAL DRIVE
DifferentialDrive::DifferentialDrive(){}
DifferentialDrive::~DifferentialDrive(){}

Eigen::MatrixXf DifferentialDrive::computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params){

    int data_dim = ticks.rows();
    Eigen::MatrixXf odometry(Eigen::MatrixXf::Zero(data_dim,3));
    Eigen::Matrix3f KL = v2t(Eigen::Vector3f(params(0), params(1), params(2)));
    Eigen::VectorXf Ko(3);
    Ko << params(3), params(4), params(5);

    Eigen::Vector3f init_robot_pose = t2v(v2t(_start_sensor_pose)*(KL.inverse()));

    odometry.row(0) = init_robot_pose;
    for (int i = 1; i < data_dim; ++i)
    {
        float t_l = (ticks(i,0) - ticks(i-1,0));
        float t_r = (ticks(i,1) - ticks(i-1,1));

        Eigen::VectorXf current_ticks(2);
        current_ticks = (ticks.row(i) - ticks.row(i-1));
        odometry.row(i) = odometry.row(i-1);
        Eigen::Vector3f step_start(odometry(i,0),odometry(i,1),odometry(i,2));
        computeOdomRobotStep(step_start, current_ticks, params);
        odometry.row(i) = step_start;
    }

    for (int i = 0; i < data_dim; ++i)
    {
        odometry.row(i) = t2v(v2t(odometry.row(i))*KL);
    }


    return odometry;
}

void DifferentialDrive::computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &ticks, const Eigen::VectorXf &params){
    //ticks are relative
    float t_l = ticks(0)*params(3);
    float t_r = ticks(1)*params(4);
    float baseline = params(5);

//    float delta_plus = t_r + t_l;
//    float delta_minus = t_r - t_l;
//    float dth = delta_minus/baseline;

//    float one_minus_cos_theta_over_theta, sin_theta_over_theta;
//    computeThetaTerms(sin_theta_over_theta, one_minus_cos_theta_over_theta, dth);
//    float dx = .5*delta_plus*sin_theta_over_theta;
//    float dy = .5*delta_plus*one_minus_cos_theta_over_theta;

//    float s = sin(pose(2));
//    float c = sin(pose(2));

//    pose(0) += c*dx - s*dy;
//    pose(1) += s*dx + c*dy;
//    pose(2) += dth;
//    normalizeTheta(pose(2));

    pose(2) += (t_r - t_l)/baseline;
    normalizeTheta(pose(2));
    float rho = (t_l + t_r)/2;
    pose(0) += rho*cos(pose(2));
    pose(1) += rho*sin(pose(2));

}

Eigen::MatrixXf DifferentialDrive::getConditioner(){

    Eigen::MatrixXf conditioner(6,6);
    conditioner = Eigen::MatrixXf::Identity(6,6);
    conditioner.block(0,0,3,3) *= 1;//lambda_laser;
    conditioner.block(3,3,2,2) *= 1;//lambda_odom;
    conditioner(5,5) *= 1;//lambda_baseline;

    return conditioner;

}

std::vector<Motion> DifferentialDrive::getMotions()
{
    std::vector<Motion> motion_library;

    Motion forward(0.2,0,0,1,0);
    Motion backward(-0.2,0,0,1,0);
    Motion arc_forw_left(0.2,0,0.2,1,0);
    Motion arc_back_left(-0.2,0,-0.2,1,0);
    Motion arc_forw_right(0.2,0,-0.2,1,0);
    Motion arc_back_right(-0.2,0,0.2,1,0);
    Motion rotate_left(0,0,0.5,0,1.4);
    Motion rotate_right(0,0,-0.5,0,1.4);

    motion_library.push_back(forward);
    motion_library.push_back(backward);
    motion_library.push_back(arc_forw_left);
    motion_library.push_back(arc_back_left);
    motion_library.push_back(arc_forw_right);
    motion_library.push_back(arc_back_right);
    motion_library.push_back(rotate_left);
    motion_library.push_back(rotate_right);

    return motion_library;
}

Eigen::Vector2f DifferentialDrive::predictTicks(const Eigen::Matrix3f& L, const Eigen::Matrix3f& KL, const Eigen::Vector3f& Ko) {
    Eigen::Matrix3f delta_o = KL*L*KL.inverse(); //from laser trasformation to robot one
    Eigen::Vector3f variations = t2v(delta_o);
    float rho = 0;

    float Dx = variations(0);
    normalizeTheta(variations(2));

    if(sin(variations(2)) > 1e-8) {
        float R = Dx/sin(variations(2));
        //double R = (pow(Dx,2) + pow(variations(1),2))/2*variations(1);
        rho = R*variations(2);
    } else {
        rho = sign_fnc(variations(0))*fabs(sqrt(pow(variations(0),2) + pow(variations(1),2)));
    }

    Eigen::Vector2f predictedTicks(Eigen::Vector2f::Zero());
    predictedTicks(0) = 1/Ko(0) * (rho - (Ko(2)/2)*variations(2));
    predictedTicks(1) = 1/Ko(1) * (rho + (Ko(2)/2)*variations(2));

    return predictedTicks;
}


/// OMNIDIRECTIONAL
Omnidirectional::Omnidirectional(){}
Omnidirectional::~Omnidirectional(){}

std::vector<Motion> Omnidirectional::getMotions()
{
    std::vector<Motion> motion_library;

    Motion forward(0.3,0,0,1,0);
    Motion backward(-0.3,0,0,1,0);
    Motion left(0,0.3,0,1,0);
    Motion right(0,-0.3,0,1,0);
    Motion diagonal_forw_right(0.3,0.3,0.05,1,0);
    Motion diagonal_back_right(-0.3,-0.3,-0.05,1,0);
    Motion diagonal_forw_left(0.3,-0.3,-0.05,1,0);
    Motion diagonal_back_left(-0.3,0.3,0.05,1,0);
    Motion rotate_left(0,0,0.5,0,1.4);
    Motion rotate_right(0,0,-0.5,0,1.4);

    motion_library.push_back(forward);
    motion_library.push_back(backward);
    motion_library.push_back(left);
    motion_library.push_back(right);
    motion_library.push_back(diagonal_forw_right);
    motion_library.push_back(diagonal_back_right);
    motion_library.push_back(diagonal_forw_left);
    motion_library.push_back(diagonal_back_left);
    motion_library.push_back(rotate_left);
    motion_library.push_back(rotate_right);

    return motion_library;
}

/// YOUBOT
Youbot::Youbot(){}
Youbot::~Youbot(){}

Eigen::MatrixXf Youbot::computeOdometrySensorFrame(const Eigen::MatrixXf &ticks, const Eigen::VectorXf &params){
    int data_dim = ticks.rows();
    Eigen::MatrixXf odometry(Eigen::MatrixXf::Zero(data_dim,3));
    Eigen::Matrix3f KL = v2t(Eigen::Vector3f(params(0), params(1), params(2)));
    Eigen::VectorXf Ko(5);
    Ko << params(3),params(4),params(5),params(6),params(7);
    Eigen::Vector3f init_robot_pose = t2v(v2t(_start_sensor_pose)*(KL.inverse()));
    odometry.row(0) = init_robot_pose;

    for (int i = 1; i < data_dim; ++i)
    {
        Eigen::VectorXf t(4);
        t = ticks.row(i) - ticks.row(i-1);
        odometry.row(i) = odometry.row(i-1);
        Eigen::Vector3f step_start(odometry(i,0),odometry(i,1),odometry(i,2));
        computeOdomRobotStep(step_start, t, params);
        odometry.row(i) = step_start;
    }
    for (int i = 0; i < data_dim; ++i)
    {
        odometry.row(i) = t2v(v2t(odometry.row(i))*KL);
    }

    return odometry;
}

void Youbot::computeOdomRobotStep(Eigen::Vector3f &pose, const Eigen::VectorXf &t, const Eigen::VectorXf &params){
    //ticks are relative

    float geom_factor = params(3);
    float w_FL = params(4);
    float w_FR = params(5);
    float w_BL = params(6);
    float w_BR = params(7);

// factory kinematics
//    float delta_longitudinal = (-t(0)*w_FL + t(1)*w_FR - t(2)*w_BL + t(3)*w_BR)/4;
//    float delta_trasversal = (t(0)*w_FL + t(1)*w_FR - t(2)*w_BL - t(3)*w_BR)/4;
//    pose(2) += (t(0)*w_FL + t(1)*w_FR + t(2)*w_BL + t(3)*w_BR)/(4*geom_factor);

    // vrep compliant kinematics
    float delta_longitudinal = (-t(0)*w_FL - t(1)*w_FR - t(2)*w_BL - t(3)*w_BR)/4;
    float delta_trasversal = (t(0)*w_FL - t(1)*w_FR - t(2)*w_BL + t(3)*w_BR)/4;
    pose(2) += (t(0)*w_FL - t(1)*w_FR + t(2)*w_BL - t(3)*w_BR)/(4*geom_factor);

    normalizeTheta(pose(2));
    pose(0) = pose(0) + delta_longitudinal*cos(pose(2)) - delta_trasversal*sin(pose(2));
    pose(1) = pose(1) + delta_longitudinal*sin(pose(2)) + delta_trasversal*cos(pose(2));
}


Eigen::MatrixXf Youbot::getConditioner(){

    Eigen::MatrixXf conditioner(Eigen::MatrixXf::Identity(8,8));
    conditioner.block(0,0,3,3) *= 1; //lambda_laser;
    conditioner(3,3) *= 1;//lambda_baselines;
    conditioner.block(4,4,4,4) *= 0;// lambda_ticks;

    return conditioner;

}

}
