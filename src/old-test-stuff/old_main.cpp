#include "ros/ros.h"

#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>
#include <time.h>

//message filters, used in synchronization procedure
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//sensor values analysis
#include <nav_msgs/Odometry.h>          //thin_scan_matcher
#include <sensor_msgs/JointState.h>     //vrep/kobuki encoders
//#include <capybarauno/capybara_ticks.h> //capybara encoders

//actuation
#include <geometry_msgs/Twist.h>        //vrep/kobuki actuation

#include "utils.h"
#include "kinematics.h"
#include "calibrator.h"
#include "viewer.h"
#include <qapplication.h>

using namespace message_filters;

using namespace common_fnc;
using namespace calibration;
using namespace kinematics;


std::vector<float> last_synchro_data;
std::vector<float> prev_synchro_data;
std::vector<std::vector<float> > real_time_log;




//keep the variables in the holy structure
struct Configure{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Configure(const std::string&, const std::vector<float>&);

    std::string robot_type;
    int data_dimension;
    int params_dimension;
    Kinematics *my_kin;
    Eigen::VectorXf init_X_o;
    std::vector<Motion> motion_library;


};


Configure::Configure(const std::string& robot_name, const std::vector<float>& params){
    Eigen::Vector3f init_laser_pose(params[0], params[1], params[2]);
    Eigen::Vector3f init_dd_params(params[3], params[4], params[5]);
    Eigen::VectorXf init_youbot_params(5);
    init_youbot_params << params[6], params[7], params[8], params[9], params[10];
    //Turtlebot
    if(robot_name == "differential_drive"){
        params_dimension = 6;
        data_dimension = 5;
        my_kin = new DifferentialDrive();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(3) = init_dd_params;
        motion_library = my_kin->getMotions();

    }
    //Marrtino
    else if(robot_name == "marrtino"){
        params_dimension = 6;
        data_dimension = 5;
        my_kin = new DifferentialDrive();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(3) = init_dd_params;
        motion_library = my_kin->getMotions();

    }
    //Youbot [TODO]
    else if(robot_name == "youbot"){
        params_dimension = 8;
        data_dimension = 7;
        my_kin = new Youbot();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(5) = init_youbot_params;
        motion_library = my_kin->getMotions();

    }
    //Vrep Pioneer
    else if(robot_name == "vrep_differential_drive"){
        params_dimension = 6;
        data_dimension = 5;
        my_kin = new DifferentialDrive();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(3) = init_dd_params;
        motion_library = my_kin->getMotions();

    }
    //Vrep Youbot
    else if(robot_name == "vrep_youbot"){
        params_dimension = 8;
        data_dimension = 7;
        my_kin = new Youbot();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(5) = init_youbot_params;
        motion_library = my_kin->getMotions();

    }
    else{
        CERR_ERR("Wrong _robot_type\n (differential_drive/marrtino/youbot)");
    }


}



//Kobuki & Thin_Scan_Matcher
void KobThin_callback(const sensor_msgs::JointStateConstPtr &encoder, const nav_msgs::Odometry::ConstPtr &abs_pose)
{
    Eigen::Vector4f quat;
    quat << abs_pose->pose.pose.orientation.w,
            abs_pose->pose.pose.orientation.x,
            abs_pose->pose.pose.orientation.y,
            abs_pose->pose.pose.orientation.z;

    last_synchro_data[0] = abs_pose->pose.pose.position.x;
    last_synchro_data[1] = abs_pose->pose.pose.position.y;
    last_synchro_data[2] = quaternion2Theta(quat);
    last_synchro_data[3] = encoder->position[0];
    last_synchro_data[4] = encoder->position[1];

    //store
    if(checkDistance(last_synchro_data,prev_synchro_data))
    {
        real_time_log.push_back(last_synchro_data);
        prev_synchro_data = last_synchro_data;
    }

}

//Vrep Pioneer dx & Thin_Scan_Matcher
void VrepThin_callback(const sensor_msgs::JointStateConstPtr &l_encoder, const sensor_msgs::JointStateConstPtr &r_encoder, const nav_msgs::Odometry::ConstPtr &abs_pose)
{
    Eigen::Vector4f quat;
    quat << abs_pose->pose.pose.orientation.w,
            abs_pose->pose.pose.orientation.x,
            abs_pose->pose.pose.orientation.y,
            abs_pose->pose.pose.orientation.z;

    last_synchro_data[0] = abs_pose->pose.pose.position.x;
    last_synchro_data[1] = abs_pose->pose.pose.position.y;
    last_synchro_data[2] = quaternion2Theta(quat);
    last_synchro_data[3] = l_encoder->position[0];
    last_synchro_data[4] = r_encoder->position[0];

    //store
    if(checkDistance(last_synchro_data,prev_synchro_data))
    {
        real_time_log.push_back(last_synchro_data);
        prev_synchro_data = last_synchro_data;
    }
}


//Vrep Youbot dx & Thin_Scan_Matcher
void Youbot_callback(const sensor_msgs::JointStateConstPtr &w1, const sensor_msgs::JointStateConstPtr &w2, const sensor_msgs::JointStateConstPtr &w3, const sensor_msgs::JointStateConstPtr &w4, const nav_msgs::OdometryConstPtr &abs_pose)
{
    Eigen::Vector4f quat;
    quat << abs_pose->pose.pose.orientation.w,
            abs_pose->pose.pose.orientation.x,
            abs_pose->pose.pose.orientation.y,
            abs_pose->pose.pose.orientation.z;

    last_synchro_data[0] = abs_pose->pose.pose.position.x;
    last_synchro_data[1] = abs_pose->pose.pose.position.y;
    last_synchro_data[2] = quaternion2Theta(quat);

    last_synchro_data[3] = w1->position[0]; //frontLEFT
    last_synchro_data[4] = w2->position[0]; //frontRIGHT
    last_synchro_data[5] = w3->position[0]; //backLEFT
    last_synchro_data[6] = w4->position[0]; //backRIGHT


    //store
    if(checkDistance(last_synchro_data,prev_synchro_data))
    {
        real_time_log.push_back(last_synchro_data);
        prev_synchro_data = last_synchro_data;
    }

}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "calibrator");
    ros::NodeHandle nh("~");
    ROS_INFO("calibrator started...");

    std::string mode,twist_topic, robot_type, pose_topic, kob_encoder_topic, vrep_l_encoder_topic, vrep_r_encoder_topic, capy_tick_topic, w1_topic, w2_topic, w3_topic, w4_topic;
    std::string filepath, param_storage;
    int DEBUG, data_block;
    float epsilon, laser_x, laser_y, laser_th, kl, kr, baseline, k_fl, k_fr, k_bl, k_br, baseline_ratio;

    //topic name params
    nh.param<std::string>("twist_topic", twist_topic, std::string("/cmd_vel_mux/input/teleop"));
    nh.param<std::string>("pose_topic", pose_topic, std::string("/odom_calib"));
    nh.param<std::string>("kobuki_ticks", kob_encoder_topic, std::string("/joint_states"));
    nh.param<std::string>("vrep_left_ticks", vrep_l_encoder_topic, std::string("/vrep/LEFT_ticks"));
    nh.param<std::string>("vrep_right_ticks", vrep_r_encoder_topic, std::string("/vrep/RIGHT_ticks"));
    nh.param<std::string>("capys_ticks", capy_tick_topic, std::string("/robot_ticks"));
    nh.param<std::string>("w1", w1_topic, std::string("/w1"));
    nh.param<std::string>("w2", w2_topic, std::string("/w2"));
    nh.param<std::string>("w3", w3_topic, std::string("/w3"));
    nh.param<std::string>("w4", w4_topic, std::string("/w4"));
    //simulation/robot params
    nh.param<std::string>("mode", mode, std::string("auto"));
    nh.param<std::string>("robot_type", robot_type, std::string("differential_drive"));
    nh.param("debug", DEBUG, 0);
    nh.param<std::string>("filepath", filepath, std::string("output_calibration.csv"));
    nh.param<std::string>("param_storage", param_storage, std::string(""));
    //calibrator params
    nh.param("data_block", data_block, 50);
    nh.param<float>("epsilon", epsilon, 1e-6);
    //init params for the sensor pose
    nh.param<float>("laser_x", laser_x, 0.05);
    nh.param<float>("laser_y", laser_y, 0.0);
    nh.param<float>("laser_th", laser_th, 0.0);
    //init params for a dd
    nh.param<float>("kl", kl, 0.03);
    nh.param<float>("kr", kr, 0.03);
    nh.param<float>("baseline", baseline, 0.3);
    //init params for an omnidirectional
    nh.param<float>("baseline_ratio", baseline_ratio, 0.42);
    nh.param<float>("k_fl", k_fl, 0.04);
    nh.param<float>("k_fr", k_fr, 0.04);
    nh.param<float>("k_bl", k_bl, 0.04);
    nh.param<float>("k_br", k_br, 0.04);


    CERR_INFO("\tTOPIC NAMEs");
    std::cerr <<"_pose_topic:="<<pose_topic<<std::endl;
    std::cerr <<"_kobuki_ticks:="<<kob_encoder_topic<<std::endl;
    std::cerr <<"_vrep_left_ticks:="<<vrep_l_encoder_topic<<std::endl;
    std::cerr <<"_vrep_right_ticks:="<<vrep_r_encoder_topic<<std::endl;
    std::cerr <<"_capys_ticks:="<<capy_tick_topic<<std::endl;
    std::cerr <<"_w1:="<<w1_topic<<std::endl;
    std::cerr <<"_w2:="<<w2_topic<<std::endl;
    std::cerr <<"_w3:="<<w3_topic<<std::endl;
    std::cerr <<"_w4:="<<w4_topic<<std::endl;
    std::cerr <<"_twist_topic:="<<twist_topic<<std::endl;
    CERR_INFO("\tSIMULATION PARAMs");
    std::cerr <<"_robot_type:="<<robot_type<<std::endl;
    std::cerr <<"_mode:="<<mode<<std::endl;
    std::cerr <<"_debug:="<<DEBUG<<std::endl;
    std::cerr <<"_filepath:="<<filepath<<std::endl;
    std::cerr <<"_param_storage:="<<param_storage<<std::endl;
    std::cerr <<"_data_block:="<<data_block<<std::endl;
    std::cerr <<"_epsilon:="<<epsilon<<std::endl;
    CERR_INFO("\tSENSOR POSE INIT PARAMs");
    std::cerr <<"_laser_x:="<<laser_x<<std::endl;
    std::cerr <<"_laser_y:="<<laser_y<<std::endl;
    std::cerr <<"_laser_th:="<<laser_th<<std::endl;
    CERR_INFO("\tDIFFERENTIAL DRIVE INIT PARAMs");
    std::cerr <<"_kl:="<<kl<<std::endl;
    std::cerr <<"_kr:="<<kr<<std::endl;
    std::cerr <<"_baseline:="<<baseline<<std::endl;
    CERR_INFO("\tYOUBOT INIT PARAMs");
    std::cerr <<"_baseline_ratio:="<<baseline_ratio<<std::endl;
    std::cerr <<"_k_fl:="<<k_fl<<std::endl;
    std::cerr <<"_k_fr:="<<k_fr<<std::endl;
    std::cerr <<"_k_bl:="<<k_bl<<std::endl;
    std::cerr <<"_k_br:="<<k_br<<std::endl;

    //message filters variables
    int queue_size = 100;
    float max_i_d = 0.01;


    message_filters::Subscriber<nav_msgs::Odometry> absolutepose_sub(nh, pose_topic, 1);
    
    //Kobuki & Thin_Scan_Matcher
    message_filters::Subscriber<sensor_msgs::JointState> encoder_sub(nh, kob_encoder_topic, 1);
    typedef sync_policies::ApproximateTime<sensor_msgs::JointState, nav_msgs::Odometry> MySyncPolicy_Kobuki;
    MySyncPolicy_Kobuki msp_kobuki(queue_size);
    msp_kobuki.setMaxIntervalDuration(ros::Duration(max_i_d));
    Synchronizer<MySyncPolicy_Kobuki> sync(MySyncPolicy_Kobuki(msp_kobuki), encoder_sub, absolutepose_sub);
    sync.registerCallback(boost::bind(&KobThin_callback, _1, _2));

    //VREP Pioneer & Thin_Scan_Matcher
    message_filters::Subscriber<sensor_msgs::JointState> vrep_l_encoder_sub(nh, vrep_l_encoder_topic, 1);
    message_filters::Subscriber<sensor_msgs::JointState> vrep_r_encoder_sub(nh, vrep_r_encoder_topic, 1);
    typedef sync_policies::ApproximateTime<sensor_msgs::JointState, sensor_msgs::JointState, nav_msgs::Odometry> MySyncPolicy_VrepPioneer;
    MySyncPolicy_VrepPioneer mps_vrep(queue_size);
    mps_vrep.setMaxIntervalDuration(ros::Duration(max_i_d));
    Synchronizer<MySyncPolicy_VrepPioneer> vrep_sync(MySyncPolicy_VrepPioneer(mps_vrep), vrep_l_encoder_sub, vrep_r_encoder_sub, absolutepose_sub);
    vrep_sync.registerCallback(boost::bind(&VrepThin_callback, _1, _2, _3));

    //VREP Youbot & Thin_Scan_Matcher
    message_filters::Subscriber<sensor_msgs::JointState> w1_sub(nh, w1_topic,1);
    message_filters::Subscriber<sensor_msgs::JointState> w2_sub(nh, w2_topic,1);
    message_filters::Subscriber<sensor_msgs::JointState> w3_sub(nh, w3_topic,1);
    message_filters::Subscriber<sensor_msgs::JointState> w4_sub(nh, w4_topic,1);
    typedef sync_policies::ApproximateTime<sensor_msgs::JointState, sensor_msgs::JointState, sensor_msgs::JointState, sensor_msgs::JointState, nav_msgs::Odometry> MySyncPolicy_YoubotVrep;
    MySyncPolicy_YoubotVrep msp_youbot(queue_size);
    msp_youbot.setMaxIntervalDuration(ros::Duration(max_i_d));
    Synchronizer<MySyncPolicy_YoubotVrep> sync_youbot(MySyncPolicy_YoubotVrep(msp_youbot), w1_sub, w2_sub, w3_sub, w4_sub, absolutepose_sub);
    sync_youbot.registerCallback(boost::bind(&Youbot_callback, _1, _2, _3, _4, _5));


    ros::Publisher cmd_vel_pub = nh.advertise<geometry_msgs::Twist>(twist_topic,1000);
    geometry_msgs::Twist command_msg;

    //init stuffs (robot/init_X_o/data_dim/params_dim etc.)
    std::vector<float> init_params;
    init_params.push_back(laser_x);
    init_params.push_back(laser_y);
    init_params.push_back(laser_th);
    init_params.push_back(kl);
    init_params.push_back(kr);
    init_params.push_back(baseline);
    init_params.push_back(baseline_ratio);
    init_params.push_back(k_fl);
    init_params.push_back(k_fr);
    init_params.push_back(k_bl);
    init_params.push_back(k_br);
    Configure rs(robot_type, init_params);

    last_synchro_data = std::vector<float>(rs.data_dimension);
    prev_synchro_data = std::vector<float>(rs.data_dimension);


    //Init Calibrator
    Calibrator my_calibrator;
    my_calibrator.setRobot(rs.my_kin);
    my_calibrator.setEpsilon(epsilon);
    my_calibrator.setInitParams(rs.init_X_o);

    int cnt = 1;
    /*
       QApplication application(argc,argv);
       Viewer viewer;
       viewer.setWindowTitle("Calibration Gui");
       viewer.show();
  */

    //timing variables
    time_t start, step;

    //If required, open file to save plot data
    std::fstream plot_file, param_storage_file;
    int PLOT = 0, STORE_PARAM = 0;
    if(strlen(filepath.c_str()))
        PLOT = 1;
    if(strlen(param_storage.c_str()))
        STORE_PARAM = 1;

    if(PLOT){
        CERR_INFO("Creating file for plot");
        plot_file.open(filepath.c_str(), std::fstream::out | std::fstream::app);
        if(!plot_file){
            CERR_ERR("Error while creating plot file");
            PLOT = 0;
        } else {
            time(&start);
            plot_file << "MODE " << mode << std::endl;
            plot_file << "ROBOT " << robot_type << std::endl;
            plot_file << "INIT_PARAM ";
            for(int i=0; i<rs.init_X_o.rows(); ++i)
                plot_file << rs.init_X_o(i) << " ";
            plot_file << std::endl;
        }
    }

    if(STORE_PARAM){
        CERR_INFO("Creating parameter storage file");
        param_storage_file.open(param_storage.c_str(), std::fstream::out);
        if(!param_storage_file){
            CERR_ERR("Error while creating param_storage_file");
            STORE_PARAM = 0;
        }
    }


    //Auto Stuff
    std::vector<Motion>::iterator motion_it = rs.motion_library.begin();
    bool manual = false, automatic = false, exploration = false, exploitation = false;
    Eigen::Vector3f init_motion_pose(Eigen::Vector3f::Zero());
    Eigen::Vector3f current_pose(Eigen::Vector3f::Zero());
    if(mode == "auto"){
        automatic = true;
        exploration = true;
    }
    else if(mode == "manual"){
        manual = true;
        std::cerr<<std::endl;
        CERR_INFO("Move the Robot, I'll collect the data");
    }
    else
        CERR_ERR("Error in the _mode(manual/auto) definition");

    int exploited_motion = -1;


    // ============ ROS::OK ============ //
    while(ros::ok())
    {

        if(manual) //drive with joystick - collect data - calibrate
        {
            int current_size = real_time_log.size();
            if(current_size > data_block*cnt)
            {
                Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));
                //convert data
                std2Eigen(real_time_log, Data);

                pose = Data.block(0,0,current_size,3);
                ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                //calibrate
                my_calibrator.calibrate(pose, ticks);
                my_calibrator.getParams(rs.init_X_o);

                // store plot data in a file
                if(PLOT){
                    time(&step);
                    double step_time = difftime(step, start);
                    Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                    plot_file << "TIME " << step_time << std::endl;
                    plot_file << "DATA " << current_size << std::endl;
                    plot_file << "PARAMS ";
                    for(int i = 0; i<rs.init_X_o.rows(); ++i)
                        plot_file << rs.init_X_o(i) << " ";
                    plot_file << std::endl;

                    plot_file << "H_MATRIX ";
                    for(int i=0; i<curr_H.rows(); ++i)
                        for(int j=0; j<curr_H.cols(); ++j)
                            plot_file << curr_H(i,j) << " ";
                    plot_file<<std::endl;
                }
                // store last computed parameters in a file
                if(STORE_PARAM){
                    param_storage_file.open(param_storage.c_str(), std::fstream::out);
                    param_storage_file << "[MODE]\n" << mode << std::endl;
                    param_storage_file << "[ROBOT]\n" << robot_type << std::endl;
                    param_storage_file << "[PARAMS]\n" << rs.init_X_o << std::endl;
                    param_storage_file.close();
                }

                std::cerr<<std::endl;
                CERR_INFO("Current Params:");
                CERR_INFO(rs.init_X_o);
                cnt++;

                /*
          //update data for the viewer
          initial_sensor_pose << pose(0,0), pose(0,1), pose(0,2);
          rs.my_kin->setStartSensorPose(initial_sensor_pose);
          Eigen::MatrixXf calibrated_pose = rs.my_kin->computeOdometrySensorFrame(ticks, rs.init_X_o);

          viewer.setGtPose(pose);
          //viewer.setCalibratedPose(calibrated_pose);
          viewer.updateData();
          //viewer.show();
          */
            }
        }
        else if(automatic) //automatic procedure
        {
            if(exploration)
            {
                if(motion_it != rs.motion_library.end())
                {
                    if(motionTerminated(init_motion_pose, current_pose, *motion_it)) //check if terminated and in case update init_motion_pose
                    {
                        init_motion_pose = current_pose;

                        int current_size = real_time_log.size();
                        if(DEBUG)
                            CERR_INFO2("Current_size: ", current_size);
                        Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                        Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                        Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));
                        
                        std2Eigen(real_time_log, Data);

                        pose = Data.block(0,0,current_size,3);
                        ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                        //calibrate
                        my_calibrator.calibrate(pose, ticks);
                        //recover and store H, eigs, eigv
                        my_calibrator.getHMatrix((*motion_it)._H);
                        my_calibrator.getEigVal((*motion_it)._eigval);
                        my_calibrator.getEigVet((*motion_it)._eigvet);

                        //clear dataset and reset initial calibrator params
                        real_time_log.clear();
                        my_calibrator.setInitParams(rs.init_X_o);

                        if(PLOT){
                            time(&step);
                            double step_time = difftime(step, start);
                            Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                            plot_file << "TIME " << step_time << std::endl;
                            plot_file << "DATA " << current_size << std::endl;
                            plot_file << "PARAMS ";
                            for(int i = 0; i<rs.init_X_o.rows(); ++i)
                                plot_file << rs.init_X_o(i) << " ";
                            plot_file << std::endl;

                            plot_file << "H_MATRIX ";
                            for(int i=0; i<curr_H.rows(); ++i)
                                for(int j=0; j<curr_H.cols(); ++j)
                                    plot_file << curr_H(i,j) << " ";
                            plot_file<<std::endl;
                        }


                        //next motion
                        ++motion_it;
                        if(DEBUG)
                            CERR_DEBUG("\nNext Motion");
                    }
                    else
                    {
                        //continue the execution
                        command_msg.linear.x = (*motion_it).getLinX();
                        command_msg.linear.y = (*motion_it).getLinY();
                        command_msg.angular.z = (*motion_it).getAng();
                        cmd_vel_pub.publish(command_msg);
                    }
                }
                else
                {
                    exploration = false;
                    exploitation = true;
                    if(DEBUG)
                        CERR_DEBUG("Starting Exploitation Phase");
                    if(PLOT)
                        plot_file << "EXPLOITATION" << std::endl;
                    //choose first motion
                    my_calibrator.reset();
                    Eigen::MatrixXf current_H = my_calibrator.getHMatrix();
                    selectNextMotion(exploited_motion, rs.motion_library, current_H);
                }

            }
            else if(exploitation)
            {
                if(true) //put here the termination condition //TODO
                {
                    if(motionTerminated(init_motion_pose, current_pose, rs.motion_library[exploited_motion]))
                    {
                        init_motion_pose = current_pose;

                        //calibrate
                        int current_size = real_time_log.size();
                        Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                        Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                        Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));
                        
                        std2Eigen(real_time_log, Data);

                        pose = Data.block(0,0,current_size,3);
                        ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                        my_calibrator.calibrate(pose, ticks);
                        my_calibrator.getParams(rs.init_X_o);
                        std::cerr<<std::endl;
                        CERR_INFO("Current Params:");
                        CERR_INFO(rs.init_X_o);

                        if(PLOT){
                            time(&step);
                            double step_time = difftime(step, start);
                            Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                            plot_file << "TIME " << step_time << std::endl;
                            plot_file << "DATA " << current_size << std::endl;
                            plot_file << "PARAMS ";
                            for(int i = 0; i<rs.init_X_o.rows(); ++i)
                                plot_file << rs.init_X_o(i) << " ";
                            plot_file << std::endl;

                            plot_file << "H_MATRIX ";
                            for(int i=0; i<curr_H.rows(); ++i)
                                for(int j=0; j<curr_H.cols(); ++j)
                                    plot_file << curr_H(i,j) << " ";
                            plot_file<<std::endl;
                        }
                        if(STORE_PARAM){
                            param_storage_file.open(param_storage.c_str(), std::fstream::out);
                            param_storage_file << "[MODE]\n" << mode << std::endl;
                            param_storage_file << "[ROBOT]\n" << robot_type << std::endl;
                            param_storage_file << "[PARAMS]\n" << rs.init_X_o << std::endl;
                            param_storage_file.close();
                        }


                        if(exploited_motion%2 != 0) //if a backward motion has been executed
                        {
                            Eigen::MatrixXf current_H = my_calibrator.getHMatrix();
                            selectNextMotion(exploited_motion, rs.motion_library, current_H);
                        }
                        else
                            exploited_motion++; //execute the backward motion

                    }
                    else //continue moving
                    {
                        command_msg.linear.x = rs.motion_library[exploited_motion].getLinX();
                        command_msg.linear.y = rs.motion_library[exploited_motion].getLinY();
                        command_msg.angular.z = rs.motion_library[exploited_motion].getAng();
                        cmd_vel_pub.publish(command_msg);
                    }
                }

            }

        }


        current_pose = Eigen::Vector3f(last_synchro_data[0], last_synchro_data[1], last_synchro_data[2]);
        usleep(10000);
        ros::spinOnce();
        //application.processEvents();
    }

}
