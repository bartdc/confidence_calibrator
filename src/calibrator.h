#pragma once

#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include "kinematics.h"
#include <iostream>

namespace calibration{

class Calibrator{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Calibrator(int iterations = 5);
    ~Calibrator();
    void calibrate(const Eigen::MatrixXf &pose, const Eigen::MatrixXf &ticks);

    inline void setInitParams(const Eigen::VectorXf& params){_X_o = params; _params_number = _X_o.size();}
    inline void setIterations(const int& it){_iterations = it;}
    inline void getParams(Eigen::VectorXf &out){out = _X_o;}
    inline void setEpsilon(float eps){_eps = eps;}
    inline void setRobot(kinematics::Kinematics* robot){_my_robot = robot;}
    inline void getHMatrix(Eigen::MatrixXf& H_mat){H_mat = _H_mat;}
    inline Eigen::MatrixXf getHMatrix(){return _H_mat;}
    inline void getEigVet(Eigen::MatrixXcf& eigvet){eigvet = _eigvet;}
    inline void getEigVal(Eigen::VectorXcf& eigval){eigval = _eigval;}
    inline float getNormalizedChi2(){if(_data_dim>0) return _chi2/_data_dim; else return 10e6;}
    void reset();

    float _min_chi2_norm;
    int _chi2_min_it;
    Eigen::VectorXf _best_X_o;

private:
    float _chi2;
    int _params_number;
    int _iterations;
    float _eps;
    Eigen::VectorXf _X_o;
    kinematics::Kinematics* _my_robot;
    int _data_dim;
    Eigen::MatrixXf _H_mat;
    Eigen::MatrixXcf _eigvet;
    Eigen::VectorXcf _eigval;

    Eigen::MatrixXf computeJacobian(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks);
    Eigen::Vector3f computeError(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks);
    Eigen::Vector3f computePerturbError(const Eigen::Matrix3f &Z, const Eigen::VectorXf &ticks, const int& id);

};
}
