#include "utils.h"
using namespace std;

namespace common_fnc{
Eigen::Matrix3f v2t(const Eigen::Vector3f &v){

    Eigen::Matrix3f A;
    float c = cos(v(2));
    float s = sin(v(2));
    float x = v(0);
    float y = v(1);
    A << c, -s,  x,
            s,  c,  y,
            0,  0,  1;

    return A;
}

Eigen::Vector3f t2v(const Eigen::Matrix3f &A){
    Eigen::Vector3f t;
    t.block(0,0,2,1) = A.block(0,2,2,1);
    t(2) = atan2(A(1,0),A(0,0));
    return t;
}

void normalizeTheta(float &theta){
    theta = atan2(sin(theta),cos(theta));
}

/**
   * @brief quaternion2Theta, this function extracts 2d orientation (theta) from a quaternion
   * @param q: quaternion [w,x,y,z]
   * @return theta (2d orientation)
   */
float quaternion2Theta(const Eigen::Vector4f &q){
    return atan2(2*(q(0)*q(3)+q(1)*q(2)),1-2*(q(2)*q(2) + q(3)*q(3)));
}

bool checkDistance(const std::vector<float> &last, const std::vector<float> &prev)
{
    float m_thresh = 0.05;
    float rad_thresh = 0.1;
    float distance = sqrt((last[0] - prev[0])*(last[0] - prev[0]) + (last[1] - prev[1])*(last[1] - prev[1]));
    float rad_dist = fabs(last[2] - prev[2]);
    if(distance > m_thresh || rad_dist > rad_thresh)
        return true; //keep the data
    else
        return false; //discard
}


bool motionTerminated(const Eigen::Vector3f &init_pose, const Eigen::Vector3f &current_pose, kinematics::Motion& curr_mot)
{
    float distance = 0;
    float compared_distance = 0;
    if(curr_mot.getLinDist() > 0) //distance as meters
    {
        distance = (pow(current_pose(0) - init_pose(0),2) + pow(current_pose(1) - init_pose(1),2));
        compared_distance = curr_mot.getLinDist();
    }
    else //distance as radiance
    {
        Eigen::Vector3f transf = t2v(v2t(current_pose).inverse()*v2t(init_pose));
        normalizeTheta(transf(2));
        distance = fabs(transf(2));
        compared_distance = curr_mot.getRadDist();
    }

    //std::cerr<<"travelled distance: "<<distance<<std::endl;
    if(distance < compared_distance)
        return false; //continue exploring the motion
    else
        return true;

}

void std2Eigen(const std::vector<std::vector<float> >& V, Eigen::MatrixXf& E)
{
    int vector_rows = V.size();
    int vector_cols = V[0].size();

    for (int i = 0; i < vector_rows; ++i)
        for (int j = 0; j < vector_cols; ++j)
            E(i,j) = V[i][j];
}


void selectNextMotion(int& id, std::vector<kinematics::Motion>& motion_library, const Eigen::MatrixXf& H){

    //init id
    id = 0;

    std::cerr<<"Selecting next motion"<<std::endl;
    float best_determinant = 0;
    for(int i=0; i < motion_library.size(); ++i){
        float current_det = (H + motion_library[i]._H).determinant();
        if( current_det > best_determinant)
        {
            id = i;
            best_determinant = current_det;
        }
    }
    std::cerr<<"Selected Motion #"<<id<<std::endl;
    std::cerr<<"linear : ["<<-motion_library[id].getLinX()<<", "<<motion_library[id].getLinY()<<", 0]"<<std::endl;
    std::cerr<<"angular: [0, 0, "<<motion_library[id].getAng()<<"]"<<std::endl;


    //little trick. If a 'backward' motion has been selected, return the respective forward motion
    if(id%2 != 0)
        id -= 1;

}


void printBanner(const char** banner) {
  const char** b = banner;
  while(*b) {
    std::cerr << *b << std::endl;
    b++;
  }
}


void printParameters(const Eigen::VectorXf& params, const std::string& robot_type){

    CERR_PARAMS("laser_x:  ",params(0));
    CERR_PARAMS("laser_y:  ",params(1));
    CERR_PARAMS("laser_th: ",params(2));

    if(robot_type == "differential_drive"){
        CERR_PARAMS("kl:       ",params(3));
        CERR_PARAMS("kr:       ",params(4));
        CERR_PARAMS("baseline: ",params(5));
    }

    if(robot_type == "youbot"){
        CERR_PARAMS("baseline: ",params(3));
        CERR_PARAMS("k_fl:     ",params(4));
        CERR_PARAMS("k_fr:     ",params(5));
        CERR_PARAMS("k_bl:     ",params(6));
        CERR_PARAMS("k_br:     ",params(7));
    }

}

void computeThetaTerms(float& sin_theta_over_theta,
               float& one_minus_cos_theta_over_theta,
               float theta) {
// evaluates the taylor expansion of sin(x)/x and (1-cos(x))/x,
// where the linearization point is x=0, and the functions are evaluated
// in x=theta
  sin_theta_over_theta=0;
  one_minus_cos_theta_over_theta=0;
  float theta_acc=1;
  for (uint8_t i=0; i<6; i++) {
    if (i&0x1)
      one_minus_cos_theta_over_theta+=theta_acc*cos_coeffs[i];
    else
      sin_theta_over_theta+=theta_acc*sin_coeffs[i];
    theta_acc*=theta;
  }
  sin_theta_over_theta=sinThetaOverTheta(theta);
  one_minus_cos_theta_over_theta=oneMinisCosThetaOverTheta(theta) ;
}

}
