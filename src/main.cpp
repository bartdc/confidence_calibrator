#include "ros/ros.h"

#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>
#include <time.h>

//message filters, used in synchronization procedure
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//sensor values analysis
#include <nav_msgs/Odometry.h>          //thin_scan_matcher
#include <sensor_msgs/JointState.h>     //vrep/kobuki encoders

//actuation
#include <geometry_msgs/Twist.h>        //vrep/kobuki actuation

#include "utils.h"
#include "kinematics.h"
#include "calibrator.h"
#include "viewer.h"
#include <qapplication.h>

using namespace message_filters;

using namespace common_fnc;
using namespace calibration;
using namespace kinematics;


std::vector<float> last_synchro_data;
std::vector<float> prev_synchro_data;
std::vector<std::vector<float> > real_time_log;




//keep the variables in the holy structure
struct Configure{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Configure(const std::string&, const std::vector<float>&);

    std::string robot_type;
    int data_dimension;
    int params_dimension;
    Kinematics *my_kin;
    Eigen::VectorXf init_X_o;
    std::vector<Motion> motion_library;


};


Configure::Configure(const std::string& robot_name, const std::vector<float>& params){
    Eigen::Vector3f init_laser_pose(params[0], params[1], params[2]);
    Eigen::Vector3f init_dd_params(params[3], params[4], params[5]);
    Eigen::VectorXf init_youbot_params(5);
    init_youbot_params << params[6], params[7], params[8], params[9], params[10];
    //Turtlebot
    if(robot_name == "differential_drive"){
        params_dimension = 6;
        data_dimension = 5;
        my_kin = new DifferentialDrive();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(3) = init_dd_params;
        motion_library = my_kin->getMotions();

    }
    else if(robot_name == "youbot"){
        params_dimension = 8;
        data_dimension = 7;
        my_kin = new Youbot();
        init_X_o = Eigen::VectorXf::Zero(params_dimension);
        init_X_o.head(3) = init_laser_pose;
        init_X_o.tail(5) = init_youbot_params;
        motion_library = my_kin->getMotions();

    }
    else{
        CERR_ERR("Wrong _robot_type\n (differential_drive/marrtino/youbot)");
    }


}



// Differential Drive & Thin_Scan_Matcher
void DiffDriveThin_callback(const sensor_msgs::JointStateConstPtr &encoder, const nav_msgs::Odometry::ConstPtr &abs_pose)
{
    int left_enc_id = 0;
    int right_enc_id = 1;

    //search for encoder id in the array
    int l = encoder->name.size();
    for(int i=0; i<l; i++)
    {
        std::string current_joint = encoder->name[i];
        if(!strcmp(current_joint.c_str(), "Pioneer_p3dx_leftMotor"))
            left_enc_id = i;
        else if(!strcmp(current_joint.c_str(), "Pioneer_p3dx_rightMotor"))
            right_enc_id = i;
    }


    Eigen::Vector4f quat;
    quat << abs_pose->pose.pose.orientation.w,
            abs_pose->pose.pose.orientation.x,
            abs_pose->pose.pose.orientation.y,
            abs_pose->pose.pose.orientation.z;

    last_synchro_data[0] = abs_pose->pose.pose.position.x;
    last_synchro_data[1] = abs_pose->pose.pose.position.y;
    last_synchro_data[2] = quaternion2Theta(quat);
    last_synchro_data[3] = encoder->position[left_enc_id];
    last_synchro_data[4] = encoder->position[right_enc_id];

    //store
    if(checkDistance(last_synchro_data,prev_synchro_data))
    {
        real_time_log.push_back(last_synchro_data);
        prev_synchro_data = last_synchro_data;
    }

}


// Youbot & Thin_Scan_Matcher
void YoubotThin_callback(const sensor_msgs::JointStateConstPtr &encoder, const nav_msgs::OdometryConstPtr &abs_pose)
{
    int fl_enc_id = -1;
    int fr_enc_id = -1;
    int bl_enc_id = -1;
    int br_enc_id = -1;

    //search for encoder id in the array
    int l = encoder->name.size();
    for(int i=0; i<l; i++)
    {
        std::string current_joint = encoder->name[i];
        if(current_joint == "rollingJoint_fl" || current_joint == "wheel_joint_fl"){
            fl_enc_id = i;
        }
        else if(current_joint == "rollingJoint_fr" || current_joint == "wheel_joint_fr"){
            fr_enc_id = i;
        }
        else if(current_joint == "rollingJoint_rl" || current_joint == "wheel_joint_bl"){
            bl_enc_id = i;
        }
        else if(current_joint == "rollingJoint_rr" || current_joint == "wheel_joint_br"){
            br_enc_id = i;

        }
    }

    if(fl_enc_id != -1)
    {
        Eigen::Vector4f quat;
        quat << abs_pose->pose.pose.orientation.w,
                abs_pose->pose.pose.orientation.x,
                abs_pose->pose.pose.orientation.y,
                abs_pose->pose.pose.orientation.z;

        last_synchro_data[0] = abs_pose->pose.pose.position.x;
        last_synchro_data[1] = abs_pose->pose.pose.position.y;
        last_synchro_data[2] = quaternion2Theta(quat);
        last_synchro_data[3] = encoder->position[fl_enc_id]; //frontLEFT
        last_synchro_data[4] = encoder->position[fr_enc_id]; //frontRIGHT
        last_synchro_data[5] = encoder->position[bl_enc_id]; //backLEFT
        last_synchro_data[6] = encoder->position[br_enc_id]; //backRIGHT

        //store
        if(checkDistance(last_synchro_data,prev_synchro_data))
        {
            real_time_log.push_back(last_synchro_data);
            prev_synchro_data = last_synchro_data;
        }
    }
}


const char* banner[]={
    "\nUnsupervised calibration tool for wheeled mobile platforms",
    " it reads encoder data and odometry data (from a matcher/tracker) and performs intrinsics and estrinsics calibration",
    "",
    "usage: confidence_calibrator calibrator [options]",
    "Options: ",
    " ***TOPIC***",
    " _twist_topic:=              [string <geometry_msgs::Twist>], the node will drive the robot for unsupervised calibration with this topic, default [/cmd_vel]",
    " _robot_encoder_topic:=      [string <sensor_msgs::JointState>], the encoder data coming from platform, default [/joint_states]",
    " _pose_topic:=               [string <nav_msgs::Odometry>], the pose of the sensor coming from a matcher or a tracker, default [/odom_calib] ",
    " ***SIMULATION***",
    " _mode:=                     [string], the mode choosen for calibration. In manual mode the calibrator will simply record data, default: [auto] {auto, manual}",
    " _robot_type:=               [string], the robot related kinematics, default: [differential_drive] {differential_drive, youbot}",
    " _debug:=                    [int], activate it if you want debug cout, default [0] {0,1}",
    " _filepath:=                 [string], stores params-H mat-eigenvalues and eigenvectors. The octave stuff will plot it automatically, default [null] {null, out.csv}",
    " _param_storage:=            [string], stores final params. Input file for odom_publisher, default [null] {null, out.csv}",
    " _data_block:=               [int], block of data stored for each calibration step (used in manual mode), default [50]",
    " _momentum:=                 [int], in auto mode, seeking for the best set of params, will avoid local minima, default [5]",
    " _epsilon:=                  [float], epsilon increment used in computing the jacobian for least-squares, default [1e-6]",
    " _chi2_threshold:=           [float], chi^2 threshold for termination condition (auto mode), default [1e-7]",
    " _max_interval_duration:=    [float], maximum interval duration for approximate synchronizer (encoder-odometry), default [0.01]",
    " ***INITIAL GUESS***",
    " _laser_x:=                  [float], initial guess, in meter, for laser x-coordinate position, default [0.05]",
    " _laser_y:=                  [float], initial guess, in meter, for laser y-coordinate position, default [0.0]",
    " _laser_th:=                 [float], initial guess, in meter, for laser orientation, default [0]",
    " _kl:=                       [float], Differential Drive. Initial guess for kl parameter. It represents the radius, in meter, of the left wheel, default [0.03]",
    " _kr:=                       [float], Differential Drive. Initial guess for kr parameter. It represents the radius, in meter, of the right wheel, default [0.03]",
    " _baseline:=                 [float], Differential Drive. Initial guess for baseline, in meter, default [0.3]",
    " _baseline_ratio:=           [float], Youbot. Initial guess for baseline ratio in case of youbot, in meter. It represent the mean of the two baselines, default [0.42]",
    " _k_fl:=                     [float], Youbot. Initial guess for k_fl parameter. It represent the radius, in meter, of the front left wheel, default [0.04]",
    " _k_fr:=                     [float], Youbot. Initial guess for k_fr parameter. It represent the radius, in meter, of the front right wheel, default [0.04]",
    " _k_bl:=                     [float], Youbot. Initial guess for k_fl parameter. It represent the radius, in meter, of the back left wheel, default [0.04]",
    " _k_br:=                     [float], Youbot. Initial guess for k_br parameter. It represent the radius, in meter, of the back right wheel, default [0.04]",
    0
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "calibrator");
    ros::NodeHandle nh("~");
    ROS_INFO("calibrator started...");

    std::string mode,twist_topic, robot_type, pose_topic, robot_encoder_topic;
    std::string filepath, param_storage;
    int DEBUG, data_block, momentum;
    float epsilon, laser_x, laser_y, laser_th, kl, kr, baseline, k_fl, k_fr, k_bl, k_br, baseline_ratio, chi2_threshold, max_i_d;


    for(int i=0; i<argc; i++){
        if(!strcmp(argv[i],"-h")){
            printBanner(banner);
            return 0;
        }
    }

    //topic name params
    nh.param<std::string>("twist_topic", twist_topic, std::string("/cmd_vel"));
    nh.param<std::string>("pose_topic", pose_topic, std::string("/odom_calib"));
    nh.param<std::string>("robot_encoder_topic", robot_encoder_topic, std::string("/joint_states"));

    //simulation/robot params
    nh.param<std::string>("mode", mode, std::string("auto"));
    nh.param<std::string>("robot_type", robot_type, std::string("differential_drive"));
    nh.param("debug", DEBUG, 0);
    nh.param<std::string>("filepath", filepath, std::string(""));
    nh.param<std::string>("param_storage", param_storage, std::string(""));
    //calibrator params
    nh.param("data_block", data_block, 50);
    nh.param("momentum", momentum, 5);
    nh.param<float>("epsilon", epsilon, 1e-6);
    nh.param<float>("chi2_threshold", chi2_threshold, 1e-7);
    nh.param<float>("max_interval_duration", max_i_d, 0.01);
    //init params for the sensor pose
    nh.param<float>("laser_x", laser_x, 0.05);
    nh.param<float>("laser_y", laser_y, 0.0);
    nh.param<float>("laser_th", laser_th, 0.0);
    //init params for a dd
    nh.param<float>("kl", kl, 0.03);
    nh.param<float>("kr", kr, 0.03);
    nh.param<float>("baseline", baseline, 0.3);
    //init params for an omnidirectional
    nh.param<float>("baseline_ratio", baseline_ratio, 0.42);
    nh.param<float>("k_fl", k_fl, 0.04);
    nh.param<float>("k_fr", k_fr, 0.04);
    nh.param<float>("k_bl", k_bl, 0.04);
    nh.param<float>("k_br", k_br, 0.04);


    CERR_INFO("============TOPIC NAMEs");
    std::cerr <<"_pose_topic:= "<<pose_topic<<std::endl;
    std::cerr <<"_robot_encoder_topic:= "<<robot_encoder_topic<<std::endl;
    std::cerr <<"_twist_topic:= "<<twist_topic<<std::endl;
    CERR_INFO("============SIMULATION PARAMs");
    std::cerr <<"_robot_type:= "<<robot_type<<std::endl;
    std::cerr <<"_mode:= "<<mode<<std::endl;
    std::cerr <<"_debug:= "<<DEBUG<<std::endl;
    std::cerr <<"_filepath:= "<<filepath<<std::endl;
    std::cerr <<"_param_storage:= "<<param_storage<<std::endl;
    std::cerr <<"_data_block:= "<<data_block<<std::endl;
    std::cerr <<"_epsilon:= "<<epsilon<<std::endl;
    std::cerr <<"_max_interval_duration:= "<<max_i_d<<std::endl;
    std::cerr <<"_chi2_threshold:= "<<chi2_threshold<<std::endl;
    std::cerr <<"_momentum:= "<<momentum<<std::endl;
    CERR_INFO("============SENSOR POSE INIT PARAMs");
    std::cerr <<"_laser_x:="<<laser_x<<std::endl;
    std::cerr <<"_laser_y:="<<laser_y<<std::endl;
    std::cerr <<"_laser_th:="<<laser_th<<std::endl;
    CERR_INFO("============DIFFERENTIAL DRIVE INIT PARAMs");
    std::cerr <<"_kl:="<<kl<<std::endl;
    std::cerr <<"_kr:="<<kr<<std::endl;
    std::cerr <<"_baseline:="<<baseline<<std::endl;
    CERR_INFO("============YOUBOT INIT PARAMs");
    std::cerr <<"_baseline_ratio:="<<baseline_ratio<<std::endl;
    std::cerr <<"_k_fl:="<<k_fl<<std::endl;
    std::cerr <<"_k_fr:="<<k_fr<<std::endl;
    std::cerr <<"_k_bl:="<<k_bl<<std::endl;
    std::cerr <<"_k_br:="<<k_br<<std::endl;

    //message filters variables
    int queue_size = 100;

    ///Synchro Procedures
    message_filters::Subscriber<nav_msgs::Odometry> absolutepose_sub(nh, pose_topic, 1);
    message_filters::Subscriber<sensor_msgs::JointState> encoder_sub(nh, robot_encoder_topic, 1);
    typedef sync_policies::ApproximateTime<sensor_msgs::JointState, nav_msgs::Odometry> SyncPolicy;
    SyncPolicy robot_synch_policy(queue_size);
    robot_synch_policy.setMaxIntervalDuration(ros::Duration(max_i_d));
    Synchronizer<SyncPolicy> synchronize(SyncPolicy(robot_synch_policy), encoder_sub, absolutepose_sub);
    if(robot_type == "differential_drive")
        synchronize.registerCallback(boost::bind(&DiffDriveThin_callback, _1, _2));
    else if(robot_type == "youbot")
        synchronize.registerCallback(boost::bind(&YoubotThin_callback, _1, _2));

    ros::Publisher cmd_vel_pub = nh.advertise<geometry_msgs::Twist>(twist_topic,1000);
    geometry_msgs::Twist command_msg;

    //init stuffs (robot/init_X_o/data_dim/params_dim etc.)
    std::vector<float> init_params;
    init_params.push_back(laser_x);
    init_params.push_back(laser_y);
    init_params.push_back(laser_th);
    init_params.push_back(kl);
    init_params.push_back(kr);
    init_params.push_back(baseline);
    init_params.push_back(baseline_ratio);
    init_params.push_back(k_fl);
    init_params.push_back(k_fr);
    init_params.push_back(k_bl);
    init_params.push_back(k_br);
    Configure rs(robot_type, init_params);

    last_synchro_data = std::vector<float>(rs.data_dimension);
    prev_synchro_data = std::vector<float>(rs.data_dimension);


    //Init Calibrator
    Calibrator my_calibrator;
    my_calibrator.setRobot(rs.my_kin);
    my_calibrator.setEpsilon(epsilon);
    my_calibrator.setInitParams(rs.init_X_o);

    int cnt = 1;
    /*
       QApplication application(argc,argv);
       Viewer viewer;
       viewer.setWindowTitle("Calibration Gui");
       viewer.show();
  */

    //timing variables
    time_t start, step;

    //If required, open file to save plot data
    std::fstream plot_file, param_storage_file;
    int PLOT = 0, STORE_PARAM = 0;
    if(strlen(filepath.c_str()))
        PLOT = 1;
    if(strlen(param_storage.c_str()))
        STORE_PARAM = 1;

    if(PLOT){
        CERR_INFO("Creating file for plot");
        plot_file.open(filepath.c_str(), std::fstream::out | std::fstream::app);
        if(!plot_file){
            CERR_ERR("Error while creating plot file");
            PLOT = 0;
        } else {
            time(&start);
            plot_file << "MODE " << mode << std::endl;
            plot_file << "ROBOT " << robot_type << std::endl;
            plot_file << "INIT_PARAM ";
            for(int i=0; i<rs.init_X_o.rows(); ++i)
                plot_file << rs.init_X_o(i) << " ";
            plot_file << std::endl;
        }
    }

    if(STORE_PARAM){
        CERR_INFO("Creating parameter storage file");
        param_storage_file.open(param_storage.c_str(), std::fstream::out);
        if(!param_storage_file){
            CERR_ERR("Error while creating param_storage_file");
            STORE_PARAM = 0;
        }
    }


    //Auto Stuff
    std::vector<Motion>::iterator motion_it = rs.motion_library.begin();
    bool manual = false, automatic = false, exploration = false, exploitation = false;
    Eigen::Vector3f init_motion_pose(Eigen::Vector3f::Zero());
    Eigen::Vector3f current_pose(Eigen::Vector3f::Zero());
    if(mode == "auto"){
        automatic = true;
        exploration = true;
    }
    else if(mode == "manual"){
        manual = true;
        std::cerr<<std::endl;
        CERR_INFO("Move the Robot, I'll collect the data");
    }
    else
        CERR_ERR("Error in the _mode(manual/auto) definition");

    int exploited_motion = -1;


    // ============ ROS::OK ============ //
    while(ros::ok())
    {

        if(manual) //drive with joystick - collect data - calibrate
        {
            int current_size = real_time_log.size();
            if(current_size > data_block*cnt)
            {
                Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));
                //convert data
                std2Eigen(real_time_log, Data);

                pose = Data.block(0,0,current_size,3);
                ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                //calibrate
                my_calibrator.calibrate(pose, ticks);
                my_calibrator.getParams(rs.init_X_o);

                // store plot data in a file
                if(PLOT){
                    time(&step);
                    double step_time = difftime(step, start);
                    Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                    plot_file << "TIME " << step_time << std::endl;
                    plot_file << "DATA " << current_size << std::endl;
                    plot_file << "PARAMS ";
                    for(int i = 0; i<rs.init_X_o.rows(); ++i)
                        plot_file << rs.init_X_o(i) << " ";
                    plot_file << std::endl;

                    plot_file << "H_MATRIX ";
                    for(int i=0; i<curr_H.rows(); ++i)
                        for(int j=0; j<curr_H.cols(); ++j)
                            plot_file << curr_H(i,j) << " ";
                    plot_file<<std::endl;
                }
                // store last computed parameters in a file
                if(STORE_PARAM){
                    param_storage_file.open(param_storage.c_str(), std::fstream::out);
                    param_storage_file << "[MODE]\n" << mode << std::endl;
                    param_storage_file << "[ROBOT]\n" << robot_type << std::endl;
                    param_storage_file << "[PARAMS]\n" << rs.init_X_o << std::endl;
                    param_storage_file.close();
                }

                std::cerr<<std::endl;
                CERR_INFO("Current Params:");
                printParameters(rs.init_X_o, robot_type);
                cnt++;

            }
        }
        else if(automatic) //automatic procedure
        {
            if(exploration)
            {
                if(motion_it != rs.motion_library.end())
                {
                    if(motionTerminated(init_motion_pose, current_pose, *motion_it)) //check if terminated and in case update init_motion_pose
                    {
                        init_motion_pose = current_pose;

                        int current_size = real_time_log.size();
                        if(DEBUG)
                            CERR_INFO2("Current_size: ", current_size);
                        Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                        Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                        Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));

                        std2Eigen(real_time_log, Data);

                        pose = Data.block(0,0,current_size,3);
                        ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                        //calibrate
                        my_calibrator.calibrate(pose, ticks);
                        //recover and store H, eigs, eigv
                        my_calibrator.getHMatrix((*motion_it)._H);
                        my_calibrator.getEigVal((*motion_it)._eigval);
                        my_calibrator.getEigVet((*motion_it)._eigvet);

                        //clear dataset and reset initial calibrator params
                        real_time_log.clear();
                        my_calibrator.setInitParams(rs.init_X_o);

                        if(PLOT){
                            time(&step);
                            double step_time = difftime(step, start);
                            Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                            plot_file << "TIME " << step_time << std::endl;
                            plot_file << "DATA " << current_size << std::endl;
                            plot_file << "PARAMS ";
                            for(int i = 0; i<rs.init_X_o.rows(); ++i)
                                plot_file << rs.init_X_o(i) << " ";
                            plot_file << std::endl;

                            plot_file << "H_MATRIX ";
                            for(int i=0; i<curr_H.rows(); ++i)
                                for(int j=0; j<curr_H.cols(); ++j)
                                    plot_file << curr_H(i,j) << " ";
                            plot_file<<std::endl;
                        }


                        //next motion
                        ++motion_it;
                        if(DEBUG)
                            CERR_DEBUG("\nNext Motion");
                    }
                    else
                    {
                        //continue the execution
                        command_msg.linear.x = (*motion_it).getLinX();
                        command_msg.linear.y = (*motion_it).getLinY();
                        command_msg.angular.z = (*motion_it).getAng();
                        cmd_vel_pub.publish(command_msg);
                    }
                }
                else
                {
                    exploration = false;
                    exploitation = true;
                    if(DEBUG)
                        CERR_DEBUG("Starting Exploitation Phase");
                    if(PLOT)
                        plot_file << "EXPLOITATION" << std::endl;
                    //choose first motion
                    my_calibrator.reset();
                    Eigen::MatrixXf current_H = my_calibrator.getHMatrix();
                    selectNextMotion(exploited_motion, rs.motion_library, current_H);
                    if(DEBUG){
                        CERR_DEBUG("exploited motion: ");
                        CERR_DEBUG(exploited_motion);
                    }
                }

            }
            else if(exploitation)
            {

                if(motionTerminated(init_motion_pose, current_pose, rs.motion_library[exploited_motion]))
                {
                    init_motion_pose = current_pose;

                    //calibrate
                    int current_size = real_time_log.size();
                    Eigen::MatrixXf Data(Eigen::MatrixXf::Zero(current_size,rs.data_dimension));
                    Eigen::MatrixXf pose(Eigen::MatrixXf::Zero(current_size,3));
                    Eigen::MatrixXf ticks(Eigen::MatrixXf::Zero(current_size,rs.data_dimension - 3));

                    std2Eigen(real_time_log, Data);

                    pose = Data.block(0,0,current_size,3);
                    ticks = Data.block(0,3,current_size,rs.data_dimension - 3);
                    my_calibrator.calibrate(pose, ticks);
                    my_calibrator.getParams(rs.init_X_o);
                    std::cerr<<std::endl;
                    CERR_INFO("Current Params:");
                    printParameters(rs.init_X_o, robot_type);
                    if(PLOT){
                        time(&step);
                        double step_time = difftime(step, start);
                        Eigen::MatrixXf curr_H = my_calibrator.getHMatrix();
                        plot_file << "TIME " << step_time << std::endl;
                        plot_file << "DATA " << current_size << std::endl;
                        plot_file << "PARAMS ";
                        for(int i = 0; i<rs.init_X_o.rows(); ++i)
                            plot_file << rs.init_X_o(i) << " ";
                        plot_file << std::endl;

                        plot_file << "H_MATRIX ";
                        for(int i=0; i<curr_H.rows(); ++i)
                            for(int j=0; j<curr_H.cols(); ++j)
                                plot_file << curr_H(i,j) << " ";
                        plot_file<<std::endl;
                    }
                    if(STORE_PARAM){
                        param_storage_file.open(param_storage.c_str(), std::fstream::out);
                        param_storage_file << "[MODE]\n" << mode << std::endl;
                        param_storage_file << "[ROBOT]\n" << robot_type << std::endl;
                        param_storage_file << "[PARAMS]\n" << rs.init_X_o << std::endl;
                        param_storage_file.close();
                    }


                    /// ******Termination Criteria
                    if(my_calibrator.getNormalizedChi2() > my_calibrator._min_chi2_norm)
                    {
                        my_calibrator._chi2_min_it++;
                        //std::cerr<<"CURRENT_IT_MIN_CHI2: "<<my_calibrator._chi2_min_it<<std::endl;
                        if(my_calibrator._chi2_min_it > momentum)
                        {
                            std::cerr<<"Automatic procedure completed!"<<std::endl;
                            //print final params
                            CERR_INFO("Final Params:");
                            printParameters(rs.init_X_o, robot_type);
                            ros::shutdown();
                        }
                    }
                    else //better set of params found
                    {
                        my_calibrator._best_X_o = rs.init_X_o;
                        my_calibrator._chi2_min_it = 0;
                        my_calibrator._min_chi2_norm = my_calibrator.getNormalizedChi2();

                    }
                    /// ****** END Termination Criteria

                    if(exploited_motion%2 != 0) //if a backward motion has been executed
                    {
                        Eigen::MatrixXf current_H = my_calibrator.getHMatrix();
                        selectNextMotion(exploited_motion, rs.motion_library, current_H);
                        if(DEBUG){
                            CERR_DEBUG("exploited motion: ");
                            CERR_DEBUG(exploited_motion);
                        }
                    }
                    else
                        exploited_motion++; //execute the backward motion

                }
                else //continue moving
                {
                    command_msg.linear.x = rs.motion_library[exploited_motion].getLinX();
                    command_msg.linear.y = rs.motion_library[exploited_motion].getLinY();
                    command_msg.angular.z = rs.motion_library[exploited_motion].getAng();
                    cmd_vel_pub.publish(command_msg);
                }


            }

        }


        current_pose = Eigen::Vector3f(last_synchro_data[0], last_synchro_data[1], last_synchro_data[2]);
        usleep(10000);
        ros::spinOnce();
        //application.processEvents();
    }

}
