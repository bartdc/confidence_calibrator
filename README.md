# confidence_calibrator

by Bartolomeo Della Corte, Maurilio Di Cicco and Giorgio Grisetti, 2016

[Lab RoCoCo](labrococo.dis.uniroma1.it)

Dept. of Computer, Control and Management Engineering

Sapienza University of Rome, Italy

----

## QUICK README ##
this package contains the following nodes:

* calibrator

the main node for calibration

* odom_publisher

this node takes as input a parameter file (generated by the calibrator), and publishes the odometry

* odom2ticks

if you have not direct access to platform encoder, but your goal is to estimate the sensor pose, this nodes generates fake ticks from the odometry published by the platform

* youbot_encoder_wrapper

due to the message type of the kuka youbot (alternate arm joints and wheel joints in the same topic), this node has to be launched before calibration

## Supported robots ##
### Differential Drive ###
any differential drive platform

### Omnidirectional ###
KUKA Youbot

## HOW TO INSTALL ##

    $ cd YOUR-ROS-PACKAGE
    $ git clone https://bartdc@bitbucket.org/bartdc/confidence_calibrator.git
    $ cd ..
	$ catkin_make --pkg confidence_calibrator

## STANDARD USAGE(you have access to platform encoders) ##
### differential drive ###
	$ rosrun confidence_calibrator calibrator _robot_tpye:=differential_drive _mode:=auto/manual
### youbot ###
	$ rosrun confidence_calibrator youbot_encoder_wrapper
	$ rosrun confidence_calibrator calibrator _robot_tpye:=youbot _mode:=auto/manual

## ALTERNATIVE USAGE(you haven't access to the encoders, and want only to calbrate the sensor pose) ##
	$ rosrun confidence_calibrator odom2ticks //this option is available only for differential_drive
	$ rosrun confidence_calibrator calibrator _mode:=auto/manual _max_interval_duration:=0.05 //the republishing of ticks has to be managed by the synchronizer
