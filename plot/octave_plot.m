#!/usr/bin/env octave -q
close all
clear
clc


## OCTAVE PLOT FILE FOR CONFIDENCE CALIBRATOR
# author: Bart

addpath 'plot_utils/' 

#read the input arguments
supervisedInput = 'youbot_supervised.csv';
unsupervisedInput = 'youbot_unsupervised.csv';
directoryOutput = 'youbot_dir';


#Load files extracting components
if ~strcmp(supervisedInput, '')
	[robot, sInitParams, sParam, sHmat] = loadFile(supervisedInput);
end

if ~strcmp(unsupervisedInput, '')
	[robot, uInitParams, uParam, uHmat, exploitation_step] = loadFile(unsupervisedInput);
end

#create directory
mkdir(directoryOutput);

#Plot comparison
plot_save_comparison(robot, directoryOutput, sInitParams, uInitParams, sParam, uParam, sHmat, uHmat, exploitation_step);
