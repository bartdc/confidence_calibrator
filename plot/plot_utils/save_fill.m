# This function plot the errorbar (computed with the variance)
function out = save_fill(LABEL, filepath, x_label, y_label, p_title, legend_1, legend_2, sTime, s, s_cov, uTime, u, u_cov)
		c = [0 0 1];

		figure(LABEL);
			s1 = s + s_cov'; s1(1) = 0; #at time 0 covariance is 0
			s2 = s - s_cov'; s2(1) = 0;
			u1 = u + u_cov'; u1(1) = 0;
			u2 = u - u_cov'; u2(1) = 0;


			fill([sTime; flipud(sTime)], [s1;flipud(s2)],c);
			hold on;

			U = [uTime; flipud(uTime)];
			U2 = [u2;flipud(u1)];
			fill(U,U2,'g');
			
		xlabel(x_label); ylabel(y_label);
		title(p_title);
		legend(legend_1, legend_2);
		grid;
		saveas(LABEL,filepath);

end
