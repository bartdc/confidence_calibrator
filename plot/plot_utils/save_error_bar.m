# This function plot the errorbar (computed with the variance)
function out = save_error_bar(LABEL, filepath, x_label, y_label, p_title, legend_1, legend_2, sTime, s, s_cov, uTime, u, u_cov)

		figure(LABEL);
			aL = errorbar(sTime,s,s_cov);
			set(aL(1),"color",'r');
			set(aL(1),"Linewidth",1);
			hold on;		
			bL = errorbar(uTime,u,u_cov);
			set(bL(1),"color",'g');
			set(bL(1),"Linewidth",1);
		xlabel(x_label); ylabel(y_label);
		title(p_title);
		legend(legend_1, legend_2);
		grid;
		saveas(LABEL,filepath);

end
