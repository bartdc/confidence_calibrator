function out = plot_save_comparison(robot, directoryOutput, sInitParams, uInitParams, sParam, uParam, sHmat, uHmat, exploitation_step)

	
	#remove exploration phase
	#reset the unsupervised time when exploitation begins
	uParam(exploitation_step:end,1) = uParam(exploitation_step:end,1)-uParam(exploitation_step-1,1);
	uParam = uParam(exploitation_step:end,:);
	uHmat = uHmat(exploitation_step:end,:);

	#add init params
 	sInitParams = [0, 0, sInitParams];
	uInitParams = [0, 0, uInitParams];
	sInitCov = zeros(1,size(sHmat,2));
	uInitCov = zeros(1,size(uHmat,2));
	sParam = [sInitParams;  sParam];
	uParam = [uInitParams; uParam];
	sHmat = [sInitCov; sHmat];
	uHmat = [uInitCov; uHmat];
	

	#check robot type
	values = 0;
	if(strcmp(robot,'differential_drive'))
		values = 6;
	end
	if(strcmp(robot,'youbot'))
		values = 8;
	end

	#labels
	TIME = 1;
	DATA_SIZE = 2;
	LASER_X = 3;
	LASER_Y = 4;
	LASER_A = 5;
	KL = 0;KR = 0;BL = 0;KFL = 0;KFR = 0;KBL = 0;KBR= 0;
	if(values == 6)
		KL = 6;
		KR = 7;
		BL = 8;
	elseif(values == 8)
		BL = 6;
		KFL = 7;
		KFR = 8;
		KBL = 9;
		KBR = 10;
	end	

	#extract variance infoes
	sH = [];
	uH = [];
	for i=1:size(sHmat,1)
		sHm = reshape(sHmat(i,:),values,values);
		sHm = inv(sHm);
		sH(:,end+1) = sqrt(diag(sHm));
	end
	for i=1:size(uHmat,1)	
		uHm = reshape(uHmat(i,:),values,values);
		uHm = inv(uHm);
		uH(:,end+1) = sqrt(diag(uHm));
	end

	#extract the respective time
	sTime = sParam(:,TIME);
	uTime = uParam(:,TIME);
	
	#extract the evolution of Laser X-coord
	sLx = sParam(:,LASER_X); sLx_cov = sH(1,:);
	uLx = uParam(:,LASER_X); uLx_cov = uH(1,:);
		#plot and save
		save_error_bar(LASER_X, strcat(directoryOutput,"/laser_x"), 'time', 'meter', 'laser x param evolution', 'supervised data', 'unsupervised data', sTime, sLx, sLx_cov, uTime, uLx, uLx_cov);
		save_fill(LASER_X*10, strcat(directoryOutput,"/fill_laser_x"), 'time', 'meter', 'laser x param evolution', 'supervised data', 'unsupervised data', sTime, sLx, sLx_cov, uTime, uLx, uLx_cov);		

	#extract the evolution of Laser Y-coord
	sLy = sParam(:,LASER_Y); sLy_cov = sH(2,:);
	uLy = uParam(:,LASER_Y); uLy_cov = uH(2,:);
		#plot and save
		save_error_bar(LASER_Y, strcat(directoryOutput,"/laser_y"), 'time', 'meter', 'laser y param evolution', 'supervised data', 'unsupervised data', sTime, sLy, sLy_cov, uTime, uLy, uLy_cov);
		save_fill(LASER_Y*10, strcat(directoryOutput,"/fill_laser_y"), 'time', 'meter', 'laser y param evolution', 'supervised data', 'unsupervised data', sTime, sLy, sLy_cov, uTime, uLy, uLy_cov);

	#extract the evolution of Laser Alpha-coord
	sLa = sParam(:,LASER_A); sLa_cov = sH(3,:);
	uLa = uParam(:,LASER_A); uLa_cov = uH(3,:);
		#plot and save
		save_error_bar(LASER_A, strcat(directoryOutput,"/laser_a"), 'time', 'meter', 'laser theta param evolution', 'supervised data', 'unsupervised data', sTime, sLa, sLa_cov, uTime, uLa, uLa_cov);
		save_fill(LASER_A*10, strcat(directoryOutput,"/fill_laser_a"), 'time', 'meter', 'laser theta param evolution', 'supervised data', 'unsupervised data', sTime, sLa, sLa_cov, uTime, uLa, uLa_cov);

	#----------------------------------------------#
	#----------DIFFERENTIAL DRIVE CASE-------------#
	#----------------------------------------------#
	if(strcmp(robot,'differential_drive'))
		#extract the evolution of KL
		sLkl = sParam(:,KL); sLkl_cov = sH(4,:);
		uLkl = uParam(:,KL); uLkl_cov = uH(4,:);
			#plot and save
		save_error_bar(KL, strcat(directoryOutput,"/kl"), 'time', 'meter', 'kl param evolution', 'supervised data', 'unsupervised data', sTime, sLkl, sLkl_cov, uTime, uLkl, uLkl_cov);
		save_fill(KL*10, strcat(directoryOutput,"/fill_kl"), 'time', 'meter', 'kl param evolution', 'supervised data', 'unsupervised data', sTime, sLkl, sLkl_cov, uTime, uLkl, uLkl_cov);

		#extract the evolution of KR
		sLkr = sParam(:,KR); sLkr_cov = sH(5,:);
		uLkr = uParam(:,KR); uLkr_cov = uH(5,:);
			#plot and save
		save_error_bar(KR, strcat(directoryOutput,"/kr"), 'time', 'meter', 'kr param evolution', 'supervised data', 'unsupervised data', sTime, sLkr, sLkr_cov, uTime, uLkr, uLkr_cov);
		save_fill(KR*10, strcat(directoryOutput,"/fill_kr"), 'time', 'meter', 'kr param evolution', 'supervised data', 'unsupervised data', sTime, sLkr, sLkr_cov, uTime, uLkr, uLkr_cov);

		#extract the evolution of Baseline
		sLbl = sParam(:,BL); sLbl_cov = sH(6,:);
		uLbl = uParam(:,BL); uLbl_cov = uH(6,:);
			#plot and save
		save_error_bar(BL, strcat(directoryOutput,"/bl"), 'time', 'meter', 'baseline param evolution', 'supervised data', 'unsupervised data', sTime, sLbl, sLbl_cov, uTime, uLbl, uLbl_cov);
		save_fill(BL, strcat(directoryOutput,"/fill_bl"), 'time', 'meter', 'baseline param evolution', 'supervised data', 'unsupervised data', sTime, sLbl, sLbl_cov, uTime, uLbl, uLbl_cov);


	#----------------------------------------------#
	#----------------------YOUBOT CASE-------------#
	#----------------------------------------------#
	elseif(strcmp(robot,'youbot'))
		#extract the evolution of Baseline-Ratio
		sLbl = sParam(:,BL); sLbl_cov = sH(4,:);
		uLbl = uParam(:,BL); uLbl_cov = uH(4,:);
			#plot and save
		save_error_bar(BL, strcat(directoryOutput,"/bl"), 'time', 'meter', 'baseline ratio param evolution', 'supervised data', 'unsupervised data', sTime, sLbl, sLbl_cov, uTime, uLbl, uLbl_cov);
		save_fill(BL*10, strcat(directoryOutput,"/fill_bl"), 'time', 'meter', 'baseline ratio param evolution', 'supervised data', 'unsupervised data', sTime, sLbl, sLbl_cov, uTime, uLbl, uLbl_cov);

		#extract the evolution of KFL
		sLkfl = sParam(:,KFL); sLkfl_cov = sH(5,:);
		uLkfl = uParam(:,KFL); uLkfl_cov = uH(5,:);
			#plot and save
		save_error_bar(KFL, strcat(directoryOutput,"/kfl"), 'time', 'meter', 'k front left param evolution', 'supervised data', 'unsupervised data', sTime, sLkfl, sLkfl_cov, uTime, uLkfl, uLkfl_cov);
		save_fill(KFL*10, strcat(directoryOutput,"/fill_kfl"), 'time', 'meter', 'k front left param evolution', 'supervised data', 'unsupervised data', sTime, sLkfl, sLkfl_cov, uTime, uLkfl, uLkfl_cov);

		#extract the evolution of KFR
		sLkfr = sParam(:,KFR); sLkfr_cov = sH(6,:);
		uLkfr = uParam(:,KFR); uLkfr_cov = uH(6,:);
			#plot and save
		save_error_bar(KFR, strcat(directoryOutput,"/kfr"), 'time', 'meter', 'k front right param evolution', 'supervised data', 'unsupervised data', sTime, sLkfr, sLkfr_cov, uTime, uLkfr, uLkfr_cov);
		save_fill(KFR, strcat(directoryOutput,"/fill_kfr"), 'time', 'meter', 'k front right param evolution', 'supervised data', 'unsupervised data', sTime, sLkfr, sLkfr_cov, uTime, uLkfr, uLkfr_cov);

		#extract the evolution of KBL
		sLkbl = sParam(:,KBL); sLkbl_cov = sH(7,:);
		uLkbl = uParam(:,KBL); uLkbl_cov = uH(7,:);
			#plot and save
		save_error_bar(KBL, strcat(directoryOutput,"/kbl"), 'time', 'meter', 'k back left param evolution', 'supervised data', 'unsupervised data', sTime, sLkbl, sLkbl_cov, uTime, uLkbl, uLkbl_cov);
		save_fill(KBL*10, strcat(directoryOutput,"/fill_kbl"), 'time', 'meter', 'k back left param evolution', 'supervised data', 'unsupervised data', sTime, sLkbl, sLkbl_cov, uTime, uLkbl, uLkbl_cov);

		#extract the evolution of KBR
		sLkbr = sParam(:,KBR); sLkbr_cov = sH(8,:);
		uLkbr = uParam(:,KBR); uLkbr_cov = uH(8,:);
			#plot and save
		save_error_bar(KBR, strcat(directoryOutput,"/kbr"), 'time', 'meter', 'k back right param evolution', 'supervised data', 'unsupervised data', sTime, sLkbr, sLkbr_cov, uTime, uLkbr, uLkbr_cov);
		save_fill(KBR*10, strcat(directoryOutput,"/fill_kbr"), 'time', 'meter', 'k back right param evolution', 'supervised data', 'unsupervised data', sTime, sLkbr, sLkbr_cov, uTime, uLkbr, uLkbr_cov);

	end

end
