function [robot, InitParams, Param, Hmat, exploitation_step] = loadG2o(filepath)

	%%-----CALIBRATOR specification---
	MODE = 'MODE';
	ROBOT = 'ROBOT';
	INIT_PARAM = 'INIT_PARAM';
	TIME = 'TIME';
	DATA = 'DATA';
	PARAMS = 'PARAMS';
	H_MATRIX = 'H_MATRIX';
	EXPLOITATION = 'EXPLOITATION';
	%%-------------------------

	%open the file
	fid = fopen(filepath, 'r');

	current_time = 0;
	current_data_num = 0;
	current_line = 0;

	while true
		%get current line
		c_line = fgetl(fid);

		%stop if EOF
		if c_line == -1
			break;
		end

		%Split the line using space as separator
		elements = strsplit(c_line,' ');

		switch(elements{1})
			case MODE
        			modality = elements{2};
			case ROBOT
        			robot = elements{2};
			case INIT_PARAM
				InitParams = extractInitParams(elements, robot);
			case TIME
			        current_time = str2double(elements{2});
			case DATA
				current_data_num = str2double(elements{2});
			case PARAMS
				Param(end+1,:) = extractParams(elements,robot,current_time,current_data_num);
				current_line++;
			case H_MATRIX
				Hmat(end+1,:) = extractHmatrix(elements, robot);
			case EXPLOITATION
				exploitation_step = current_line;

			otherwise
				disp('Error in reading first element');
		end
	end

end

function out = extractInitParams(elements, robot)
  values = 0;
  if(strcmp(robot,'differential_drive'))
	values = 6;
  elseif(strcmp(robot,'youbot'))
	values = 8;
  end

  for i=1:values
	out(end+1) = str2double(elements{i+1});
  end
end

function out = extractParams(elements,robot,current_time,current_data_num)
  values = 0;
  if(strcmp(robot,'differential_drive'))
	values = 6;
  elseif(strcmp(robot,'youbot'))
	values = 8;
  end

  out(1) = current_time;
  out(end+1) = current_data_num;
  
  for i=1:values
	out(end,end+1) = str2double(elements{i+1});
  end

end



function out = extractHmatrix(elements, robot)
  values = 0;
  if(strcmp(robot,'differential_drive'))
	values = 6;
  elseif(strcmp(robot,'youbot'))
	values = 8;
  end

  for i=1:values*values
	out(end+1) = str2double(elements{i+1});
  end

end

